#!/usr/bin/python
import ev3dev.ev3 as ev3
import os


#######################################################################
# Constants
#######################################################################
NAME_FILE = 'botnames.txt'


def main():
	ev3.Sound.speak("Good morning").wait()

	# Getting locations of things
	scriptPath = os.path.realpath(__file__)
	[scriptFolder,scriptName] = os.path.split(scriptPath)
	namePath = os.path.join(scriptFolder,NAME_FILE)

	botNum = int(os.environ['BOT_NUM'])
	botName = getBotName(namePath,botNum)

	
	# Update git
	# Doesn't work cuz wifi isn't connected instantly
	# ev3.Sound.speak("%s is pulling from git" % (botName)).wait()
	# command = "git -C %s pull" % (scriptFolder)
	# print "sh: %s" % (command)
	# print os.popen(command).read()

	ev3.Sound.speak("Hey, %s is ready" % (botName)).wait()


def getBotName(filePath,botNum):
	# Open file for bot names
	fileObj = open(filePath)
	fileLines = fileObj.read().split('\n')

	botString = fileLines[botNum-1]

	if botString:
		return botString
	else:
		return str(botNum)

#######################################################################
# Entry point
#######################################################################
if __name__ == '__main__':
	main()
