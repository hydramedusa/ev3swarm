import termios, fcntl, sys, os
from Ant import Ant

ant = Ant()

fd = sys.stdin.fileno()

oldterm = termios.tcgetattr(fd)
newattr = termios.tcgetattr(fd)
newattr[3] = newattr[3] & ~termios.ICANON & ~termios.ECHO
termios.tcsetattr(fd, termios.TCSANOW, newattr)

oldflags = fcntl.fcntl(fd, fcntl.F_GETFL)
fcntl.fcntl(fd, fcntl.F_SETFL, oldflags | os.O_NONBLOCK)

print 'Enter [WASD][UIOJKLM,.][+-]'
leftEffort = rightEffort = 0
speed = 32
try:
    while 1:
        try:
            c = sys.stdin.read(1)

            # Speed
            if c == '-' or c == '_':
                speed /= 2.0
                speed = min(max(speed,0),100)
                print 'Speed = %d' % (speed)
            elif c == '=' or c == '+':
                speed *= 2.0
                speed = min(max(speed,0),100)
                print 'Speed = %d' % (speed)
            # Forward
            elif c == 'w' or c == 'i':
                leftEffort = 1
                rightEffort = 1
            # Pan left
            elif c == 'a' or c == 'j':
                leftEffort = -1
                rightEffort = 1
            # Pan right
            elif c == 'd' or c == 'l':
                leftEffort = 1
                rightEffort = -1
            # Backwards
            elif c == 's' or c == ',':
                leftEffort = -1
                rightEffort = -1
            # Turn left
            elif c == 'u':
                leftEffort = 0
                rightEffort = 1
            # Turn right
            elif c == 'o':
                leftEffort = 1
                rightEffort = 0
            # Reverse left
            elif c == 'm':
                leftEffort = 0
                rightEffort = -1
            # Reverse right
            elif c == '.':
                leftEffort = -1
                rightEffort = 0
            else:
                leftEffort = rightEffort = 0

            print "Got character %s" % (c)
        except IOError:
            pass

        ant.setLeftSpeed(leftEffort * speed)
        ant.setRightSpeed(rightEffort * speed)
finally:
    termios.tcsetattr(fd, termios.TCSAFLUSH, oldterm)
    fcntl.fcntl(fd, fcntl.F_SETFL, oldflags)
