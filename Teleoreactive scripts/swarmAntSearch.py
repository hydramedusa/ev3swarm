#!/usr/bin/python
from TruthDatabase import TruthDatabase
from Ant import Ant
from RobotMover import RobotMover
from Detector import Detector
from SynchronisedCommunicator import SynchronisedCommunicator
import time
import signal
import threading
import termios, fcntl, sys, os
from collections import deque
# from getch import getch

#######################################################################
# Constants
#######################################################################
DEBUG = True
SAVE_DECISIONS = False
TIME_CHECK = False

CALIBRATE = True
CAL_TIME = 1

#######################################################################
# Globals
#######################################################################
RUNNING = True

db = None
nest = None
ant = None
robotMover = None
detector = None
comms = None
threads = []

# Debugging
iteration = 0
commandList = deque(['Decisions:'])
decision = ''

#######################################################################
# Main
#######################################################################
def main():
    global DEBUG, RUNNING, iteration, decision, commandList
    init()
    print 'Running...'

    start = time.time()
    while RUNNING:
        if TIME_CHECK:
            print "Main TR: tic"
            tic = time.time()

        db.applyRules()
        gatherFood()
        if DEBUG:
            # If last decision is different, then record it
            if commandList[-1] != decision:
                currTime = time.time() - start
                if SAVE_DECISIONS:
                    commandList.append(decision)
                else:
                    commandList[0] = decision

                # print 'Iteration: %d took %.2f seconds' % (iteration, end-start)
                printed = False
                while not printed:
                    try:
                        print '\n\n%s' % (db)
                        printed = True
                    except AttributeError:
                        pass

                print '[%6ss]:%s' % (currTime,decision)

            decision = ''

        if TIME_CHECK:
            print "Main TR: toc = %.2f" % (time.time()-tic)

        if ant.downPressed():
            RUNNING = False

        time.sleep(0)

    cleanup()

def init():
    global db, nest, ant, robotMover, detector, comms

    # Ctrl-C handler
    signal.signal(signal.SIGINT, ctrlC)

    ant = Ant()

    if CALIBRATE:
        ant.calibrate(CAL_TIME)

    db = TruthDatabase() 

    robotMover = RobotMover(ant,db)
    thread = threading.Thread(target=robotMover.threadRun,args=(stopFunc,))
    thread.start()
    threads.append(thread)
    print 'Robot Mover started'

    detector = Detector(ant,db)
    thread = threading.Thread(target=detector.threadRun,args=(stopFunc,))
    thread.start()
    threads.append(thread)
    print 'Detector started'

    comms = SynchronisedCommunicator(ant,db)
    thread = threading.Thread(target=comms.threadRun,args=(stopFunc,))
    thread.start()
    threads.append(thread)
    print 'Communicator started'

    nest = db.getNest()


def stopFunc(exitCode):
    global RUNNING
    if exitCode:
        print 'Thread caught error !!'
    RUNNING = False

def cleanup():
    print 'Cleaning up'

    robotMover.threadStop()
    detector.threadStop()
    comms.threadStop()

    print 'Waiting for threads to stop'
    for thread in threads:
        while thread.isAlive():
            time.sleep(0.001)

    if DEBUG and SAVE_DECISIONS:
        print ''
        i = 0
        for command in commandList:
            print '%4d: %s' % (i,command)
            i += 1

def gatherFood():
    global decision
    location = db.At()
    Communicating = db.isTrue('Communicating')
    Tapped = db.isTrue('Tapped')
    exploreTarget = db.ExploreTarget()
    DetectedRobot = db.Detected('Robot')
    DetectedFood = db.Detected('Food')
    HoldingFood = db.Holding('Food')
    FacingNest = db.isTrue('FacingNest')

    if Communicating:
        decision += 'COMMUNICATING so WAIT'

    elif Tapped:
        decision += 'TAPPED so COMMUNICATE'
        communicate()

    elif location == nest and HoldingFood:
        decision += 'AT NEST and HOLDING FOOD so PUTDOWN, '
        putdown()

    elif HoldingFood:
        decision += 'HOLDING FOOD so MOVETO NEST, '
        moveTo(nest)

    elif DetectedFood and location != nest:
        decision += 'NOT HOLDING FOOD and DETECTED FOOD AWAY FROM NEST so PICKUP FOOD, '
        ant.say('Found Food!')
        pickup('Food')

    elif DetectedRobot:
        decision += 'DETECTED ROBOT so HALT'
        db.set('Halt',True)

    elif db.FoodBeyond(nest):
        decision += 'FOOD OUTSIDE OF NEST so GOTOFOOD in NEST, '
        goToFood(nest)

    elif exploreTarget:
        if location == exploreTarget:
            decision += 'AT EXPLORED TARGET %s' % (exploreTarget.getID())
            db.setExploreTarget(None)
        else:
            decision += 'EXPLORE NODE %s FIRST, ' % (exploreTarget.getID())
            moveTo(exploreTarget)

    elif not db.Explored(location):
        subtreeID = location.getID()
        decision += 'AT %s AND IT IS NOT EXPLORED so EXPLORE %s, ' % (subtreeID, subtreeID)
        explore(location)

    elif location != nest:
        decision += 'ALL EXPLORED at %s and NOT AT NEST so GO TOWARDS NEST, ' % (location.getID())
        moveTo(nest)

    elif FacingNest:
        decision += 'ALL EXPLORED and AT NEST but FACING NEST so TURN'
        db.set('UTurn',True)
        db.set('Move',False)

    else:
        decision += 'NOTHING ELSE TO DO so SEARCH AGAIN'
        db.set('ResetNest',True)

#######################################################################
# Functions
#######################################################################
# Set up communicator to send messages
def communicate():
    global decision
    db.set('Tapped',False)
    db.set('Communicating',True)

# Pickup given object
def pickup(obj):
    global decision
    decision += 'PICKED UP'
    ant.setLED('RIGHT','RED')
    db.set('UTurn',False)
    db.set('Move',False)
    db.setHeld(obj)

# Put down held object
def putdown():
    global decision
    decision += 'PLACED DOWN'
    ant.setLED('RIGHT','GREEN')
    db.set('UTurn',False)
    db.set('Move',False)
    db.setHeld(None)

# Move to any tree node in the same tree
def moveTo(node):
    global decision
    if db.At(node):
        decision += 'AT NODE %s so KEEP GOING' % (node.getID())
    elif db.At().ancestorOf(node):
        decision += 'AT ANCESTOR OF %s so MOVE DOWN, ' % (node.getID())
        moveDown()
    else:
        decision += 'AT DESCENDANT OF %s so MOVE UP, ' % (node.getID())
        moveUp()

# Move downwards (away from nest)
def moveDown():
    global decision
    if not db.isTrue('FacingNest'):
        decision += 'NOT FACING NEST so MOVE'
        db.set('UTurn',False)
        db.set('Move',True)

    else:
        decision += 'FACING NEST so TURN'
        db.set('Move',False)
        db.set('UTurn',True)

# Move upwards towards the root node
def moveUp():
    global decision
    if db.isTrue('FacingNest'):
        decision += 'FACING NEST so MOVE'
        db.set('Move',True)
        db.set('UTurn',False)

    else:
        decision += 'NOT FACING NEST so TURN'
        db.set('UTurn',True)
        db.set('Move',False)

# Go to food in the sub tree. Prioritises food in the same branch as the robot.
def goToFood(node):
    global decision
    # If there is a child that has food that also is descendant of current node
    for child in node.getChildren():
        if db.ContainsFood(child) and db.At().ancestorOf(child):
            decision += "AT ANCESTOR OF SUBTREE %s THAT CONTAINS FOOD so GOTOFOOD in %s, " % (child.getID(),child.getID())
            goToFood(child)
            return

    # If there is a child that has food
    for child in node.getChildren():
        if db.ContainsFood(child):
            decision += "SUBTREE %s CONTAINS FOOD so GOTOFOOD in %s, " % (child.getID(),child.getID())
            goToFood(child)
            return

    decision += "NO CHILDREN WITH FOOD so GOTO %s, " % (node.getID())
    moveTo(node)

# Fully explore the node. Assumes that node is not a dead end
def explore(node):
    global decision
    children = node.getChildren()
    # If this node has children
    if not children:
        decision += "NO CHILDREN so MOVE DOWN, "
        # Just move down for intesection
        moveDown()
    else:
        for child in node.getChildren(): 
            if not db.Explored(child): 
                decision += "CHILD %s UNEXPLORED so GOTO %s, " % (child.getID(),child.getID()) 
                moveTo(child) 
                return 

#######################################################################
# Auxiliary
#######################################################################
def ctrlC(signal,frame):
    global RUNNING
    RUNNING = False
    print 'Ctrl+C detected'

#######################################################################
# Entry point
#######################################################################
if __name__ == '__main__':
        main()
