import ev3dev.ev3 as ev3
import time

DEBUG = True

# http://www.ev3dev.org/docs/drivers/
# http://www.ev3dev.org/docs/drivers/tacho-motor-class/
#   In python: stop_action is stop_command
# Try: http://stackoverflow.com/questions/2675028/list-attributes-of-an-object
class Ant(object):
    ###################################################################
    # Constructors
    ###################################################################
    def __init__(self):
        # Sensors
        self.touch = ev3.TouchSensor(); assert self.touch.connected
        self.gyro  = ev3.GyroSensor(); assert self.gyro.connected
        self.color = ev3.ColorSensor(); assert self.color.connected
        self.ultra = ev3.UltrasonicSensor(); assert self.ultra.connected

        self.ultra.mode = ev3.UltrasonicSensor.MODE_US_DIST_CM
        self.gyro.mode = ev3.GyroSensor.MODE_GYRO_ANG
        self.color.mode = ev3.ColorSensor.MODE_COL_REFLECT

        # Actuators
        self.touchMotor = ev3.Motor('outA'); assert self.touchMotor.connected
        self.setTouchMotorSpeed(50)
        self.setTouchMotorToBrake()
        self.leftMotor = ev3.LargeMotor('outB'); assert self.leftMotor.connected
        self.rightMotor = ev3.LargeMotor('outC'); assert self.rightMotor.connected

        self.leftMotor.duty_cycle_sp = 0
        self.rightMotor.duty_cycle_sp = 0
        self.leftMotorDirect = False
        self.rightMotorDirect = False

        self.gyroOffset = 0
        self.prevGyro = 0;

        self.setLED('LEFT','GREEN')
        self.setLED('RIGHT','GREEN')

        self.prevIntensity = self.color.value()
        self.sound = ev3.Sound()

        self.button = ev3.Button()


    def __del__(self):
        self.setLeftSpeed(0)
        self.setRightSpeed(0)

    def calibrate(self,period):
        # Gyro
        temp = self.gyro.mode
        self.gyro.mode = ev3.GyroSensor.MODE_GYRO_CAL
        time.sleep(period)
        self.gyro.mode = temp

        ## Set zero position of touch motor
        # Save previous speed
        temp = self.touchMotor.duty_cycle_sp
        
        # Move forward to prevent locking up from wires
        self.setTouchMotorSpeed(-30)
        self.touchMotor.run_forever()
        time.sleep(0.5)
        # Move towards stopper
        self.setTouchMotorSpeed(40)
        self.touchMotor.run_forever()
        time.sleep(1)
        # While motor is still running
        while self.touchMotor.speed != 0:
            # Do nothing
            pass
        # Stop trying to move if can't move further
        self.stopTouchMotor()
        # Set this position as reference
        self.touchMotor.position = 0

        # Restore previous speed
        self.setTouchMotorSpeed(temp)

    ###################################################################
    # Wheel control
    ###################################################################
    def setLeftSpeed(self,speed):
        if not self.leftMotorDirect:
            self.leftMotor.run_direct()
            self.leftMotorDirect = True
        self.leftMotor.duty_cycle_sp = speed

    def setRightSpeed(self,speed):
        if not self.rightMotorDirect:
            self.rightMotor.run_direct()
            self.rightMotorDirect = True
        self.rightMotor.duty_cycle_sp = speed

    def moveLeftByAt(self,pos,speed):
        if self.leftMotorDirect:
            self.leftMotor.stop()
            self.leftMotorDirect = False
        self.leftMotor.run_to_rel_pos(position_sp = pos, duty_cycle_sp = speed, stop_command = u'hold')
        
    def moveRightByAt(self,pos,speed):
        if self.rightMotorDirect:
            self.rightMotor.stop()
            self.rightMotorDirect = False
        self.rightMotor.run_to_rel_pos(position_sp = pos, duty_cycle_sp = speed, stop_command = u'hold')

    def waitLeftToStop(self):
        while self.leftMotor.speed != 0:
            time.sleep(0.001)

    def waitRightToStop(self):
        while self.rightMotor.speed != 0:
            time.sleep(0.001)


    ###################################################################
    # Touch system
    ###################################################################
    def pollTouch(self):
        return self.touch.value(0)

    def moveTouchMotorRel(self,pos):
        self.touchMotor.run_to_rel_pos(position_sp = pos, duty_cycle_sp = 50)

    def moveTouchMotorAbs(self,pos):
        self.touchMotor.run_to_abs_pos(position_sp = pos, duty_cycle_cp = 50)

    def setTouchMotorSpeed(self,speed):
        self.touchMotor.duty_cycle_sp = speed

    def setTouchMotorToCoast(self):
        self.touchMotor.run_to_rel_pos(position_sp = 0, stop_command = u'coast')

    def setTouchMotorToBrake(self):
        self.touchMotor.run_to_rel_pos(position_sp = 0, stop_command = u'brake')

    def setTouchMotorToHold(self):
        self.touchMotor.run_to_rel_pos(position_sp = 0, stop_command = u'hold')

    def stopTouchMotor(self):
        self.touchMotor.stop()

    ###################################################################
    # Sensors
    ###################################################################
    def getGyroAng(self):
        val = None
        warned = False
        while val == None:
            try:
                val = self.gyro.value(0)
            except IOError:
                # Stop movement until done
                self.leftMotor.duty_cycle_sp = 0
                self.rightMotor.duty_cycle_sp = 0
                self.gyroOffset += self.prevGyro
                self.prevGyro = 0
                if DEBUG and not warned:
                    print 'Ant:Wating for gyro to reconnect'
                    warned = True

            # Release processor for threads
            time.sleep(0)

        self.prevGyro = val

        return (time.time(), val + self.gyroOffset)

    def getIntensity(self, new = True):
        if new:
            self.prevIntensity = self.color.value()
            
        return self.prevIntensity

    # Returns distance in metres
    def getUSDist(self):
        return self.ultra.value(0) / 100.0

    ###################################################################
    # Other functions
    ###################################################################
    def downPressed(self):
        return self.button.down

    def leftPressed(self):
        return self.button.left

    def rightPressed(self):
        return self.button.right

    def upPressed(self):
        return self.button.up

    def beep(self):
        self.sound.beep()
    
    def say(self,words):
        self.sound.speak(words)

    def setLED(self,lr,rg):
        if lr == 'LEFT' and rg == 'RED':
            ev3.Leds.set_color(ev3.Leds.LEFT, ev3.Leds.RED)
        elif lr == 'LEFT' and rg == 'GREEN':
            ev3.Leds.set_color(ev3.Leds.LEFT, ev3.Leds.GREEN)
        elif lr == 'RIGHT' and rg == 'RED':
            ev3.Leds.set_color(ev3.Leds.RIGHT, ev3.Leds.RED)
        elif lr == 'RIGHT' and rg == 'GREEN':
            ev3.Leds.set_color(ev3.Leds.RIGHT, ev3.Leds.GREEN)
