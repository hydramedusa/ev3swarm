from Ant import Ant
from SynchronisedCommunicator import SynchronisedCommunicator

a = Ant()
a.calibrate(1)
c = SynchronisedCommunicator(a,None)

print 'Enter ternary data on every line and end with anything else:'

ternaryMsg = []
stillTernary = True
while stillTernary:
	try:
		ternary = int(raw_input())
		if ternary in range(3):
			ternaryMsg.append(ternary)
		else:
			stillTernary = False
	except ValueError:
		stillTernary = False

print 'Waiting for contact'
while not a.pollTouch():
	pass
print 'Sending %s' % (ternaryMsg)
c.sendTernary(ternaryMsg)

print 'Sent'

a.setTouchMotorToCoast()