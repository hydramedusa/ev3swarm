from TreeNode import TreeNode
import traceback
import time
import random
from collections import deque

class TruthDatabase(object):
    ###################################################################
    # Constructors
    ###################################################################
    def __init__(self):
        self.nest = TreeNode({}) # Root of tree/world map
        self.location = self.nest
        self.held = None
        self.detected = {}
        self.exploreTarget = None
        self.facts = {} # Closed world assumption

        self.set('FacingNest',False)

        self.running = False

        random.seed()

    def __str__(self):
        string = '%s:\n' % (__name__)
        string += 'At node %s\n' % (self.location.getID())
        string += 'Holding %s\n' % (self.held)
        for key in self.detected.keys():
            string += 'Detected %s: %s\n' % (key,self.detected[key])
        string += '%s\n' % (self.nest)
        string += 'Facts:'
        for key in self.facts.keys():
            string += '\t%s:%s\n' % (key,self.facts[key])
        return string

    def getNest(self):
        return self.nest

    # Returns a list of base 3 numbers which denotes the shortest path from root to food
    # Returns empty list if none exists
    def getFoodPath(self):
        frontier = deque([(self.nest,[])])
        while frontier:
            front = frontier.popleft()

            # If the front has food, then just return its path
            if self.HasFood(front[0]):
                return front[1]

            # Otherwise we expand the node and add it to the end of the frontier
            children = front[0].getChildren()
            for childNum in range(len(children)):
                frontier.append((children[childNum],front[1] + [childNum]))

        # If we reached here, then no nodes had food

        # If this node was explored, then there was actually no food
        if self.Explored(self.location):
            return []
        else:
            # Otherwise we need to send a bad message to denote further exploration
            return None

    def setFoodPath(self,path):
        currNode = self.nest
        # If there was no path, then there was no food down this branch
        if not path:
            # Breadth first search to mark explored
            
            frontier = deque([self.location])
            # While frontier isn't empty
            while frontier:
                node = frontier.popleft()

                children = node.getChildren()
                for child in children:
                    frontier.append(child)

                container = node.getValue()
                container['Explored'] = True
        else:
            while path:
                branchNumber = path.pop(0)
                children = currNode.getChildren()
                if children:
                    currNode = children[branchNumber]
                else:
                    currNode.addChild(TreeNode({}))
                    currNode.addChild(TreeNode({}))
                    currNode.addChild(TreeNode({}))
                    currNode = currNode.getChild(branchNumber)

            container = currNode.getValue()
            container['Food'] = True

    def resetNest(self):
        container = self.nest.getValue()
        container.clear()
        self.nest.deleteAllChildren()

    def setHeld(self,obj):
        self.held = obj

    def setDetected(self,obj,value):
        self.detected[obj] = value

    def setExploreTarget(self,obj):
        self.exploreTarget = obj

    #######################################################################
    # Rules
    #######################################################################
    # Maintains rules
    def applyRules(self):
        tempUTurned = self.isTrue('UTurned')
        tempTurned = self.isTrue('Turned')
        tempFacingNest = self.isTrue('FacingNest')
        tempDeadEnd = self.isTrue('DeadEnd')
        tempLocation = self.location

        # Adding food
        # If Detected('Food') then current node has food
        if self.Detected('Food'):
            self.setDetected('Food',False)
            container = tempLocation.getValue()
            container['Food'] = True

        foodNodes = self.HasFood()

        # Removing food
        # If we have left the node in the nest direction without food,
        # then it no longer has food
        if tempTurned and not self.Holding('Food') and tempFacingNest and self.HasFood(tempLocation):
            container = tempLocation.getValue()
            container['Food'] = False

        # UTurn rules
        if tempUTurned and tempFacingNest:
            self.set('FacingNest',False)
            self.set('UTurned',False)
        elif tempUTurned:
            self.set('FacingNest',True)
            self.set('UTurned',False)

        # Turn rules
        if tempTurned and tempFacingNest and \
            not tempLocation.getParent():
            RuntimeError('Turned, but at root node facing nest!')

        elif tempTurned and tempFacingNest and \
            tempLocation.childNumber()==2:

            self.location = self.At().getParent()
            self.set('Turned',False)

        elif tempTurned and tempFacingNest:
            parent = tempLocation.getParent()
            self.location = parent.getChild(tempLocation.childNumber()+1)
            self.set('FacingNest',False)
            self.set('Turned',False)

        elif tempTurned and tempLocation.empty():
            newLoc = TreeNode({})
            tempLocation.addChild(newLoc)
            tempLocation.addChild(TreeNode({}))
            tempLocation.addChild(TreeNode({}))
            self.setExploreTarget(random.choice(tempLocation.getChildren()))
            self.location = newLoc
            self.set('Turned',False)

        elif tempTurned:
            self.location = tempLocation.getChild(0)
            self.set('Turned',False)

        # Dead end rules
        if tempDeadEnd and tempFacingNest:
            self.location = self.nest
            self.set('DeadEnd',False)
            self.set('FacingNest',False)
        elif tempDeadEnd:
            container = tempLocation.getValue()
            container['DeadEnd'] = True
            tempLocation.deleteAllChildren()
            self.set('DeadEnd',False)
            self.set('FacingNest',True)

        # Reseting map
        if self.isTrue('ResetNest'):
            self.resetNest()
            self.set('ResetNest',False)

    #######################################################################
    # Predicates
    #######################################################################
    def At(self,loc = None):
        if loc:
            return self.location == loc
        else:
            return self.location

    def Holding(self,obj = None):
        if obj:
            return self.held == obj
        else:
            return self.held

    def Detected(self,obj = None):
        if obj:
            if obj in self.detected:
                return self.detected[obj]
            return False
        else:
            return self.detected.keys()

    def ExploreTarget(self, obj = None):
        if obj:
            return self.exploreTarget == obj
        else:
            return self.exploreTarget

    def FoodBeyond(self,node):
        children = node.getChildren()
        for child in children:
            if self.ContainsFood(child):
                return True

        return False

    def ContainsFood(self,node):
        if self.HasFood(node):
            return True
        else:
            children = node.getChildren()
            for child in children:
                if self.ContainsFood(child):
                    return True

        return False
        

    def HasFood(self,node=None):
        # If node was given, return truthfulness
        if node:
            container = node.getValue()
            if 'Food' in container and container['Food']:
                return True
            else:
                return False
        else: # Return all nodes with food
            frontier = [self.nest]
            foodNodes = []

            ## DFS
            # While there are elements in frontier
            while frontier:
                # Get first node
                node = frontier.pop(0)
                # Append children 
                frontier = node.getChildren() + frontier
                
                if self.HasFood(node):
                    foodNodes.append(node)

            return foodNodes

    def Explored(self,node):
        container = node.getValue()
        if 'Explored' in container:
            return True
        elif 'DeadEnd' in container:
            return True
        else:
            children = node.getChildren()
            # If there are children, if any are unexplored, then this node is unexplored
            if children:
                for child in children:
                    if not self.Explored(child):
                        return False

                container['Explored'] = True
                return True
            # There must be children if it isn't a dead end, so it's not explored yet
            else:
                return False

    def DeadEnd(self,node):
        if 'DeadEnd' in node.getValue():
            return True
        else:
            return False

    #######################################################################
    # Atoms
    #######################################################################
    def isTrue(self,fact):
        try:
            return self.facts[fact]
        except KeyError:
            return False

    def set(self,fact,val):
        self.facts[fact] = val