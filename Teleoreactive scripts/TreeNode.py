# Generalised to work with n-ary trees
class TreeNode(object):
    treeNodes = []

    @staticmethod
    def getAvailableID():
        try:
            return TreeNode.treeNodes.index(None)
        except ValueError:
            return None

    ###################################################################
    # Constructors
    ###################################################################
    def __init__(self,value):
        nodeID = TreeNode.getAvailableID()
        if nodeID == None:
            self.nodeID = len(TreeNode.treeNodes)
            TreeNode.treeNodes.append(self)
        else:
            self.nodeID = nodeID
            TreeNode.treeNodes[nodeID] = self
        self.parent=None
        self.value=value
        self.children=[]

        

    def __str__(self):
        return '\n'.join(self.stringList())

    ###################################################################
    # Functions
    ###################################################################
    def stringList(self):
        strList = []
        # If there are any children
        if self.children:
            strList.append(str(self.getID()) + ' -> ' + str(self.value) + ' {')
            for child in self.children:
                childStringList = child.stringList()
                for childString in childStringList:
                    strList.append('\t' + childString)
            strList.append('}')
        else:
            strList.append(str(self.getID()) + ' -> ' + str(self.value))

        return strList

    def getParent(self):
        return self.parent

    def getID(self):
        # If it has a parent,
        if self.parent:
            idString = self.parent.getID() + str(self.childNumber())
        else:
            idString = 'X'

        return idString

    def getValue(self):
        return self.value

    def addChild(self,child):
        TreeNode.classCheck(child)

        child.parent = self
        self.children.append(child)

    # Removes the nth child from the tree node.
    def removeChild(self,n):
        self.childCheck(n)

        child = self.children.pop(n)
        child.parent = None

        return child

    # Removes and returns children as a list
    def removeChildren(self):
        children = self.children
        for child in children:
            child.parent = None
        self.children = []

        return children

    # Gets the nth child from the tree node.
    # From 0 to k-1
    def getChild(self,n):
        self.childCheck(n)

        return self.children[n]

    # Returns a list of children
    def getChildren(self):
        return list(self.children)

    # Returns the number of children
    def numChildren(self):
        return len(self.children)

    # Returns the child number of this node to its parent
    def childNumber(self):
        if self.parent:
            return self.parent.children.index(self)
        else:
            return 0

    def deleteAll(self):
        for child in self.children:
            child.deleteAll()

        TreeNode.treeNodes[self.nodeID] = None
        del self.children
        del self.parent
        del self.value

    def deleteAllChildren(self):
        for child in self.children:
            child.deleteAll()

        self.children = []

    ###################################################################
    # Predicates
    ###################################################################
    def empty(self):
        if self.children:
            return False

        return True

    def contains(self,value):
        if self.value == value:
            return True
        else:
            for child in self.children:
                if child.contains(value):
                    return True

        return False

    # Returns whether or not this tree node is an ancestor of the
    # descendant
    def ancestorOf(self,descendant):
        TreeNode.classCheck(descendant)

        # If there are any children
        if self.children:
            if descendant in self.children:
                return True
            else:
                # Otherwise recursively check for each children
                for child in self.children:
                    if child.ancestorOf(descendant):
                        return True

        return False

    ###################################################################
    # Auxiliary
    ###################################################################
    def childCheck(self,n):
        if n < 0 or n >= len(self.children):
            raise ValueError('Child number %d doesn''t exist. There are %d children' % (n,len(self.children)))

    @staticmethod
    def classCheck(obj):
        if not isinstance(obj,TreeNode):
            raise TypeError('A %s object must be used' % (TreeNode.__name__))
