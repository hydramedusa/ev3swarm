from Ant import Ant
from SynchronisedCommunicator import SynchronisedCommunicator

a = Ant()
a.calibrate(1)
c = SynchronisedCommunicator(a,None)

print 'Waiting for contact'
while not a.pollTouch():
	pass
print 'Recieving'
binaryMsg = c.receiveBits()

print 'Recieved %s' % (binaryMsg)

a.setTouchMotorToCoast()