from Ant import Ant
from SynchronisedCommunicator import SynchronisedCommunicator

a = Ant()
a.calibrate(1)
c = SynchronisedCommunicator(a,None)

print 'Enter binary data on every line and end with anything else:'

binaryMsg = []
stillBinary = True
while stillBinary:
	try:
		binary = int(raw_input())
		if binary in range(2):
			binaryMsg.append(bool(binary))
		else:
			stillBinary = False
	except ValueError:
		stillBinary = False

print 'Waiting for contact'
while not a.pollTouch():
	pass
print 'Sending %s' % (binaryMsg)
c.sendBits(binaryMsg)

print 'Sent'

a.setTouchMotorToCoast()