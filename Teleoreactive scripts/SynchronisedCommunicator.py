import time
import mdpc
import traceback

TIME_CHECK = False

class SynchronisedCommunicator(object):
    ONE_POS = -140
    ZERO_POS = -40
    NEUTRAL_POS = -90
    LISTEN_POS = -120
    IGNORE_POS = 0

    IGNORE_TIME = 5.0

    HANDSHAKE_PERIOD = 0.1

    COMM_SLEEP = 0.1

    COMM_SPEED = 50

    TRANSFER_PERIOD = 0.75

###################################################################
# Constructors
###################################################################
    def __init__(self,ant,db):
        self.ant = ant
        ant.setTouchMotorToHold()
        self.moveToListen()
        self.ant.setTouchMotorSpeed(SynchronisedCommunicator.COMM_SPEED)

        self.db = db

        self.running = False

        self.lastCommTime = None

###################################################################
# Thread
###################################################################
    def threadStop(self):
        self.running = False

    def threadRun(self,processHalt):
        self.running = True
        try:
            while self.running:
                if TIME_CHECK:
                    print "SynchronisedCommunicator: tic"
                    tic = time.time()

                if self.lastCommTime != None and time.time()-self.lastCommTime>SynchronisedCommunicator.IGNORE_TIME:
                    self.moveToListen()
                    self.lastCommTime = None
                if self.db.isTrue('Communicating') and self.db.isTrue('FacingNest'):
                    # Sender
                    self.sendPath()
                    self.moveToIgnore()
                    self.lastCommTime = time.time()
                    self.db.set('Communicating',False)

                elif self.db.isTrue('Communicating'):
                    # Receiver
                    self.receivePath()
                    self.moveToIgnore()
                    self.lastCommTime = time.time()
                    self.db.set('Communicating',False)

                if TIME_CHECK:
                    print "SynchronisedCommunicator: toc = %.2f" % (time.time()-tic)

                time.sleep(SynchronisedCommunicator.COMM_SLEEP)
        except:
            traceback.print_exc()
            processHalt(1)

###################################################################
# Message formatting
###################################################################
    def sendPath(self):
        ternaryPath = self.db.getFoodPath()
        if ternaryPath == None:
            # Intentionally send a bad message
            self.sendBits([True, True, True, True, False, False, True, False, False, False])
        else:
            self.sendTernary(ternaryPath)


    def receivePath(self):
        ternaryPath = self.receiveTernary()
        print 'Received path: %s' % (ternaryPath)
        if ternaryPath != None:
            self.db.setFoodPath(ternaryPath)

###################################################################
# Basic operations
###################################################################
    # Sends ternary data using message encoding and parity
    def sendTernary(self,ternaryMsg):
        quaternaryMsg = [ternary + 1 for ternary in ternaryMsg]
        binaryMsg = SynchronisedCommunicator.baseConvert(quaternaryMsg,4,2)

        (message,parity) = mdpc.encode2dParity(binaryMsg)

        self.handshake(True)

        self.sendRawBits(binaryMsg)
        print "Sent binary message: %s" % (binaryMsg)
        self.sendRawBits([False, False])
        print "Sent End: %s" % ([False, False])
        self.sendRawBits(parity)
        print "Sent parity message: %s" % (parity)

    # Receives ternary data using message encoding and parity
    def receiveTernary(self):
        self.handshake(False)

        # Receieve message part
        binaryMsg = []
        notEnd = True
        while notEnd:
            bits = self.receiveRawBits(2)
            if bits[0] or bits[1]:
                binaryMsg += bits
            else:
                notEnd = False
        print "Received binary message: %s" % (binaryMsg)

        # Receieve parity part
        numParityBits = mdpc.numParity(binaryMsg)
        parity = self.receiveRawBits(numParityBits)
        print "Received parity message: %s" % (parity)

        binaryMsg = mdpc.parityCheck(binaryMsg,parity)

        # Then Message was corrupted etc
        if binaryMsg == None:
            return None
        quaternaryMsg = SynchronisedCommunicator.baseConvert(binaryMsg,2,4)

        return [quaternary - 1 for quaternary in quaternaryMsg]

    def sendString(self,string):
        bitString = SynchronisedCommunicator.stringToBits(string)
        print 'Listen to %d bytes (%d bits)' % (len(string),8*len(string))
        print 'Bits:' + str(bitString)
        self.sendBits(bitString)

    def receiveString(self,numBytes):
        bitString = self.receiveBits(8*numBytes)
        print 'Bits:' + str(bitString)
        return SynchronisedCommunicator.bitsToString(bitString)

    def sendBits(self,bits):
        self.handshake(True)

        self.sendRawBits(bits)

    def receiveBits(self,numBits):
        self.handshake(False)

        return self.receiveRawBits(numBits)

    def handshake(self,isSender):
        # Move arm forward for 0.5s
        self.moveToOne()
        time.sleep(SynchronisedCommunicator.HANDSHAKE_PERIOD)
        self.ant.stopTouchMotor()

        if isSender:
            self.ant.setTouchMotorSpeed(100)
            self.moveToZero()
            self.ant.setTouchMotorSpeed(SynchronisedCommunicator.COMM_SPEED)

        # Wait while touching
        while self.ant.pollTouch():
            pass

        if not isSender:
            self.ant.setTouchMotorSpeed(100)
            self.moveToZero()
            self.ant.setTouchMotorSpeed(SynchronisedCommunicator.COMM_SPEED)

    def sendRawBits(self,bits):
        startTime = time.time()

        numBits = len(bits)
        for bitNum in range(numBits):
            nextBitTime = bitNum*SynchronisedCommunicator.TRANSFER_PERIOD + SynchronisedCommunicator.TRANSFER_PERIOD/2 + startTime
            time.sleep(nextBitTime - time.time())

            self.sendBit(bits[bitNum],SynchronisedCommunicator.TRANSFER_PERIOD/2)

    def receiveRawBits(self,numBits):
        startTime = time.time()

        bits = [False] * numBits
        for bitNum in range(numBits):
            nextBitTime = bitNum*SynchronisedCommunicator.TRANSFER_PERIOD + SynchronisedCommunicator.TRANSFER_PERIOD/2 + startTime
            time.sleep(nextBitTime - time.time())

            bits[bitNum] = self.receiveBit(SynchronisedCommunicator.TRANSFER_PERIOD/2)

        return bits
        
    ###################################################################
    # Bit send/receive
    ###################################################################
    def sendBit(self,bit,t):
        endTime = time.time() + t
        if bit:
            self.moveToOne()
        time.sleep(endTime - time.time())
        self.moveToZero()

    def receiveBit(self,t):
        endTime = time.time() + t
        self.moveToNeutral()
        bit = False
        while time.time() < endTime:
            if self.ant.pollTouch():
                bit = True
                self.moveToZero()
                sleepTime = endTime - time.time()
                if sleepTime > 0:
                    time.sleep(sleepTime)
                break
        else:
            self.moveToZero()

        return bit

    ###################################################################
    # Movements
    ###################################################################
    def moveToOne(self):
        self.ant.moveTouchMotorAbs(SynchronisedCommunicator.ONE_POS)

    def moveToZero(self):
        self.ant.moveTouchMotorAbs(SynchronisedCommunicator.ZERO_POS)

    def moveToNeutral(self):
        self.ant.moveTouchMotorAbs(SynchronisedCommunicator.NEUTRAL_POS)

    def moveToListen(self):
        self.ant.moveTouchMotorAbs(SynchronisedCommunicator.LISTEN_POS)

    def moveToIgnore(self):
        self.ant.moveTouchMotorAbs(SynchronisedCommunicator.IGNORE_POS)

    ###################################################################
    # Type conversions
    ###################################################################
    @staticmethod
    def stringToBits(string):
        bits=[]
        for byte in bytearray(string,'ascii'):
            for bitNum in range(8):
                if byte & 128:
                    bits.append(True)
                else:
                    bits.append(False)
                byte <<= 1

        return bits

    @staticmethod
    def bitsToString(bits):
        byteList = []
        numBits = 0
        byte = 0
        while len(bits):
            byte <<= 1
            if bits.pop(0):
                byte += 1
            numBits += 1

            if numBits == 8:
                byteList.append(int(byte))
                numBits = 0
                byte = 0

        return bytearray(byteList).decode('ascii')

    @staticmethod
    def baseConvert(bits,base1,base2):
        result = []
        numBits = 0
        num = 0
        if base1 == 2 and base2 == 4:
            while bits:
                num <<= 1
                if bits.pop(0):
                    num += 1
                numBits += 1

                if numBits == 2:
                    result.append(int(num))
                    numBits = 0
                    num = 0

        elif base1 == 4 and base2 == 2:
            while bits:
                num = bits.pop(0)
                for bitNum in range(2):
                    if num & 2:
                        result.append(True)
                    else:
                        result.append(False)
                    num <<= 1


        return result
