from TruthDatabase import TruthDatabase
import traceback
import time
from collections import deque

DEBUG = True
TIME_CHECK = False

class RobotMover(object):

    white = 45.0
    black = 11.0
    food = 75.0
    middle = (white+black)/2
    intensityRange = white-black
    turnThreshold = 0.07 # If detected intensity is in the bottom or top percent of the range, we turn
    kp = 0.5;
    ki = 0.01;
    kd = 0.1;
    startSpeed = 30
    maxSpeed = 100
    gyroTurnThreshold = 55
    gyroDeadEndThreshold = 150
    deadEndTimeLimit = 5.5
    turnTimeLimit = 3.5
    bufferSublistSize = 3
    travelForwardTime = 1.7
    def __init__(self,ant,db):
        self.ant = ant

        self.db = db
        self.running = False

        self.prevError = 0
        self.integral = 0
        self.gyroDeadEndBuffer = deque([self.ant.getGyroAng()])
        self.gyroTurnBuffer = deque(self.gyroDeadEndBuffer)
        self.travelledForward = False
        self.forwardStartTime = 0
        self.driving = True
        self.stoppingTimestamp = 0
        self.turningLeftCounter = 0
        self.turningRightCounter = 0

        self.calibrate()
        
    def calibrate(self): 
        #Do read

        debugCalibrate = False

        # Get Black
        timer = time.time()+1
        val = []
        while( timer > time.time() ):
            val.append(self.ant.getIntensity())
        
        RobotMover.black = min(val) + 2
        print 'Black has been calibrated to be at %.2f' % (RobotMover.black)
        
        # Move over
        self.ant.setLeftSpeed(-25)
        time.sleep(1)
       
        if debugCalibrate:
            self.ant.setLeftSpeed(0)
            time.sleep(3)
            self.ant.setLeftSpeed(-25)

        # Get White
        timer = time.time()+1
        val2 = []
        while(timer > time.time()):
            val2.append(self.ant.getIntensity())
        
        if debugCalibrate:
            self.ant.setLeftSpeed(0)
            time.sleep(3)
        
        self.ant.setLeftSpeed(25)
        timer = time.time()+1
        while(timer > time.time()):
            val2.append(self.ant.getIntensity())

        
        if debugCalibrate:
            self.ant.setLeftSpeed(0)
            time.sleep(3)
            self.ant.setLeftSpeed(25)

        RobotMover.white = mean(val2) - 7
        print 'White has been calibrated to be at %.2f' % (RobotMover.white)
    
        RobotMover.middle = (RobotMover.white+RobotMover.black)/2
        RobotMover.intensityRange = RobotMover.white-RobotMover.black
        time.sleep(1)
    def threadStop(self):
        self.running = False

    def threadRun(self,processHalt):
        self.running = True
        try:
            while self.running:
                if TIME_CHECK:
                    print "RobotMover: tic"
                    tic = time.time()

                if self.db.isTrue('Communicating') or self.db.Detected('Robot'):
                    self.halt()    
                elif self.db.isTrue('Move'):
                    self.move()
                    self.driving = True
                elif self.db.isTrue('UTurn'):
                    self.uturn()
                    self.driving = True
                else:
                    self.halt()

                if TIME_CHECK:
                    print "RobotMover: toc = %.2f" % (time.time()-tic)

                # Release processor for threads
                time.sleep(0)
        except:
            traceback.print_exc()
            processHalt(1)

    def move(self):
        self.updateGyroBuffer()

        self.travelledForward = False

        # Set new values for this control loop
        intensity = self.ant.getIntensity()
        # Map food as the path to continue path following in a normal fashion
        if intensity >= RobotMover.food:
            intensity = RobotMover.black

        currError = RobotMover.middle - intensity
        self.integral = self.integral + currError
        if self.integral > 1000:
            self.integral = 1000
        if currError*self.prevError < 0:
            integral = 0
        derivative = currError - self.prevError

        # Actual controller
        controlEffort = RobotMover.kp*(currError) + RobotMover.ki*self.integral + RobotMover.kd*derivative

        self.prevError = currError
        # Positive error = too much to right (on black)
        # Negative error = too much to left (on white)

        # Turning right
        if (intensity >= RobotMover.white-RobotMover.intensityRange*RobotMover.turnThreshold):
            self.turningRightCounter = self.turningRightCounter + 1
            self.turningLeftCounter = 0
            rightMotorSpeed = 0
            leftMotorSpeed = clamp(RobotMover.startSpeed + self.turningRightCounter,-RobotMover.maxSpeed,RobotMover.maxSpeed*0.6)
            self.prevError = 0
            # Turning left
        elif (intensity <= RobotMover.black+RobotMover.intensityRange*RobotMover.turnThreshold):
            self.turningLeftCounter = self.turningLeftCounter + 1
            self.turningRightCounter = 0
            rightMotorSpeed = RobotMover.startSpeed/3
            leftMotorSpeed = clamp(-RobotMover.startSpeed - self.turningLeftCounter,-RobotMover.maxSpeed,RobotMover.maxSpeed)
            self.prevError = 0
        else:
            # Cruising
            self.turningLeftCounter = 0
            self.turningRightCounter = 0
            rightMotorSpeed = clamp(RobotMover.startSpeed+controlEffort,-RobotMover.maxSpeed,RobotMover.maxSpeed)
            leftMotorSpeed = clamp(RobotMover.startSpeed-controlEffort,-RobotMover.maxSpeed,RobotMover.maxSpeed)

        self.ant.setRightSpeed(rightMotorSpeed)
        self.ant.setLeftSpeed(leftMotorSpeed)

        self.detectTurnAndDeadEnds()

    def uturn(self):
        self.updateGyroBuffer()

        # Turn unless we detect that we have done a 180
        currGyro = self.firstNBufferMean(self.gyroTurnBuffer,RobotMover.bufferSublistSize)
        prevGyro = self.lastNBufferMean(self.gyroDeadEndBuffer,RobotMover.bufferSublistSize)
        if(currGyro-prevGyro >= 120):
            # We did a 180
            self.gyroDeadEndBuffer = deque()
            self.gyroTurnBuffer = deque()

            self.db.set('UTurned',True)
            self.travelledForward = False
            if DEBUG:
                print '----->RobotMover: U-turn Performed !!'
        else:
            if self.travelledForward == False:
                self.travelledForward = True
                self.gyroDeadEndBuffer = deque()
                self.gyroTurnBuffer = deque()
                self.forwardStartTime = time.time()
                rightMotorSpeed = RobotMover.startSpeed*0.7
                leftMotorSpeed = RobotMover.startSpeed*1.4
            elif time.time() < self.forwardStartTime + RobotMover.travelForwardTime:
                rightMotorSpeed = RobotMover.startSpeed*0.7
                leftMotorSpeed = RobotMover.startSpeed*1.4
            else:
                rightMotorSpeed = 0
                leftMotorSpeed = RobotMover.startSpeed*1.5
                
            self.ant.setRightSpeed(rightMotorSpeed)
            self.ant.setLeftSpeed(leftMotorSpeed)

    def detectTurnAndDeadEnds(self):
        # Detect turns and dead ends
        currGyro = self.firstNBufferMean(self.gyroTurnBuffer,RobotMover.bufferSublistSize)
        prevGyroTurn = self.lastNBufferMean(self.gyroTurnBuffer,RobotMover.bufferSublistSize)
        prevGyroDeadEnd = self.lastNBufferMean(self.gyroDeadEndBuffer,RobotMover.bufferSublistSize)
        # Assume the gyro is set to clockwise positive
        if (prevGyroTurn-currGyro >= RobotMover.gyroTurnThreshold):
            # We turned left
            self.db.set('Turned',True)
            self.gyroDeadEndBuffer = deque()
            self.gyroTurnBuffer = deque()
            if DEBUG:
                print '----->RobotMover: Turn Found !!'

        elif (currGyro-prevGyroDeadEnd >= RobotMover.gyroDeadEndThreshold):
            # We turned 180 to the right
            self.db.set('DeadEnd',True)
            self.gyroDeadEndBuffer = deque()
            self.gyroTurnBuffer = deque()
            if DEBUG:
                print '----->RobotMover: DeadEnd Found !!'

    def halt(self):
        if self.driving == True:
            self.ant.setRightSpeed(0)
            self.ant.setLeftSpeed(0)
            self.stoppingTimestamp = time.time()
            self.driving = False
        
    ########################################3##########################
    # Buffer operations
    ########################################3##########################
    def updateGyroBuffer(self):
        newGyro = self.ant.getGyroAng()
        currTime = newGyro[0]
        # Shift over times if the robot was stopped for a bit
        # This is so that the robot can stop in the middle of a turn but still detect the whole turn once it starts moving again
        if self.driving == False:
            shiftTime = currTime - self.stoppingTimestamp
            self.shiftTimes(shiftTime)
        
        self.gyroDeadEndBuffer.appendleft(newGyro)
        self.gyroTurnBuffer.appendleft(newGyro)
        # For Dead End Buffer
        while self.gyroDeadEndBuffer[-1][0] + RobotMover.deadEndTimeLimit < currTime:
            self.gyroDeadEndBuffer.pop()
        # For Turn Buffer
        while self.gyroTurnBuffer[-1][0] + RobotMover.deadEndTimeLimit < currTime:
            self.gyroTurnBuffer.pop()

    def shiftTimes(self,shiftTime):
        # shift all times in all buffers by the shiftTime
        newBuffer = deque()
        for x in self.gyroDeadEndBuffer:
            newBuffer.append((x[0]+shiftTime,x[1]))
        self.gyroDeadEndBuffer = newBuffer

        newBuffer = deque()
        for x in self.gyroTurnBuffer:
            newBuffer.append((x[0]+shiftTime,x[1]))
        self.gyroTurnBuffer = newBuffer

    def firstNBufferMean(self,buff,n):
        mean = 0
        index = 0.0
        for tup in buff:
            mean = mean*index + tup[1]
            index += 1
            mean /= index
            if index == n:
                break
        return mean

    def lastNBufferMean(self,buff,n):
        buff.reverse()
        mean = 0
        index = 0.0
        for tup in buff:
            mean = mean*index + tup[1]
            index += 1
            mean /= index
            if index == n:
                break

        buff.reverse()
        return mean

def clamp(n, floor, ceiling):
    return max(floor, min(n, ceiling))

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers),1)

