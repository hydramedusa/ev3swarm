from TruthDatabase import TruthDatabase
import traceback
import time

DEBUG = True
#MAX_DETECT_THRESHOLD = 0.075 # m
MAX_ROBOT_THRESHOLD = 2#1.5 #This seems to be a relatively safe limit
DETECT_RATE = 0.025 # Detection resolution

ULTRA_PASS_THRESH = 10 # 0.25
TOUCH_PASS_THRESH = 2 # 0.05
INTENSITY_PASS_THRESH = 1

TIME_CHECK = False

class Detector(object):
    def __init__(self,ant,db):
        self.ant = ant

        self.db = db
        self.running = False

    def threadStop(self):
        self.running = False

    def threadRun(self,processHalt):
        self.running = True
        notifiedIntensity = False
        notifiedDistance = False

        ultraPasses = float('inf')
        touchPasses = float('inf')
        intensityPasses = float('inf')

        try:
            while self.running:
                if self.db.isTrue('Communicating'):
                    pass
                else:
                    if TIME_CHECK:
                        print "Detector: tic"
                    tic = time.time()

                    ultraPasses += 1
                    touchPasses += 1
                    intensityPasses += 1

                    if ultraPasses >= ULTRA_PASS_THRESH:
                        ultraPasses = 0
                        dist = self.ant.getUSDist()

                        if  dist < MAX_ROBOT_THRESHOLD:
                            self.db.setDetected('Robot', True)
                            self.ant.setLED('LEFT','RED')
                        else:
                            self.db.setDetected('Robot',False)
                            self.ant.setLED('LEFT','GREEN')

                    if touchPasses >= TOUCH_PASS_THRESH:
                        touchPasses = 0

                        if self.ant.pollTouch():
                            self.db.set("Tapped",True)

                    if intensityPasses >= INTENSITY_PASS_THRESH:
                        intensityPasses = 0

                        intensity = self.ant.getIntensity(False)
                    
                        if intensity > 75:
                            self.db.setDetected('Food', True)
                        else:
                            self.db.setDetected('Food',False)
                        
                    if TIME_CHECK:
                        print "Detector: toc = %.2f" % (time.time()-tic)

                # Release processor for threads
                time.sleep(DETECT_RATE)
        except:
            traceback.print_exc()
            processHalt(1)