import math

DEBUG = True

# Codes and messages are both lists of Booleans in this file

# baseLength: The length of the message in one dimenion of the two.
#     This should be ceil(sqrt(len(message))) at minimum.
def encode2dParity(originalMsg,baseLength=None):
    if baseLength == None:
        baseLength = numParity(originalMsg)/2
    elif not dimensionLengthValid(len(originalMsg),baseLength,2):
        raise ValueError('baseLength is not large enough for message')

    # Pad original message with 0s
    paddedMsg = getPaddedMessage(originalMsg,baseLength)

    (rowSum,colSum) = sum2d(paddedMsg,baseLength)
    parityBits = [rowSum[i]%2==1 for i in range(baseLength)] + [colSum[i]%2==1 for i in range(baseLength)]

    # Append parity bits to message
    return (paddedMsg,parityBits)

# Given a binary list message and binary list parityBits
def parityCheck(originalMsg,parityBits):
    numParityBits = len(parityBits)
    # If there is an odd number of parity bits, or that if the sum of column and row parity bits are odd
    if numParityBits%2==1 or sum(parityBits)%2==1:
        # Something went wrong
        return None

    baseLength = numParityBits / 2

    paddedMsg = getPaddedMessage(originalMsg,baseLength)

    (rowSum,colSum) = sum2d(paddedMsg,baseLength)

    expectedParityBits = [rowSum[i]%2==1 for i in range(baseLength)] + [colSum[i]%2==1 for i in range(baseLength)]

    errors = []
    for parityBitNum in range(len(parityBits)):
        errors.append(parityBits[parityBitNum] != expectedParityBits[parityBitNum])

    # If either dimensions show more than one error, then we can't say anything
    rowErrSum = sum(errors[0:baseLength])
    colErrSum = sum(errors[baseLength:])
    if rowErrSum == 1 and colErrSum == 1:
        # Correct error
        row = errors[0:baseLength].index(True)
        col = errors[baseLength:].index(True)
        index = baseLength*row + col
        # If the error was in a location outside of message length
        if index >= len(originalMsg):
            # Parity bits are wrong
            return None
        # Flip incorrect bit
        originalMsg[index] = not originalMsg[index]
        
        return originalMsg
    elif rowErrSum == 0 or colErrSum == 0:
        return originalMsg
    else:
        return None


# Returns a tuple of two lists of the sum in each dimension and a total sum
def sum2d(message,baseLength):
    colSum = [0]*baseLength
    rowSum = [0]*baseLength
    for row in range(baseLength):
        for col in range(baseLength):
            colSum[col] += message[baseLength*row + col]
            rowSum[row] += message[baseLength*row + col]

    return (rowSum,colSum)

def numParity(message):
    return 2*int(math.ceil(math.sqrt(len(message))))

def dimensionLengthValid(msgLen,dimLen,numDim):
    return msgLen <= dimLen**numDim

def getPaddedMessage(message,baseLength):
    return message + [False]*(baseLength*baseLength - len(message))