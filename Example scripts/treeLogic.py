#!/usr/bin/env python

tree=[]

def treeInit():
    pass


class tree:
    #Structure is
    #[Parent, [Child], [Child]]
    
    it = 0
    value=[]
    def __init__(self):
        self.value=[0,None,None]
        it = 1
    
    
    def get_parent(curr, tree=None):
        if(tree is None): #first call with no tree, so need to recurse with a tree. Insert our own tree
            if( curr == self.value[0]):
                return 0
            else:
                #Recurse with our stored tree
                x = get_parent(curr,self.value[1])
                if( x == None):
                    x = get_parent(curr,self.value[2])
                
                if( x == 0 ):
                    return self.value[0]
                else:
                    return x
                
        else:
            #You have passed me a tree, probs a recursive call. If the current value is the value you gave me, pass 0 back
            if( tree[0] == curr):
                return 0 #Found the node, need to return it so parent call will know
            else:
                #Set the default return to None
                x = None
                if(tree[1] is not None): #If tree 1 isn't empty, recursive call on it
                    x = get_parent(curr,tree[1])
                    
                if( x == None): #If tree 1 didn't have my value, try it on tree 2
                    if(tree[2] is not None): 
                        x = get_parent(curr,tree[2])
                #Tree 2 can return None = not found, 0 = the next one is child, so I am parent, or another value which will be the parent value
                if( x == 0 ):
                    return tree[0] #If zero, I am parent, so pass my value back
                else:
                    return x #Either None which means not found or a paret node with differnt value
        
        
    def add_branch(self,parent,leaf):
        #Similar to add node, but adds a node and a branch and the path you are currently traversng
        add_node(self,parent,leaf) #Adds in a node along the path
        add_node(self.parent,None) #Adds in the branch
    
    #Returns the new node it put in, which should be set as parent
    def add_node(self, parent, leaf=None):
        #Leaf is used if you are adding a node in the path of an existing tree
        if( self.value[0]!= parent):
            #It isn't a parent, recurse using next level parents
            x =  self.add_node(self.value[1],leaf)
            if( x == None):
                return self.add_node(self.value[2],leaf)
        else:
            #It is a parent now
            if( leaf == None):
                #This is a new node that will be a leaf
                #Add it to any children nodes
                if(self.value[1]==None):
                    self.value[1] = self.it
                    self.it +=1
                    return self.it - 1
                elif(self.value[2]==None):
                    self.value[2] = self.it;
                    self.it +=1
                    return self.it - 1
                else:
                    print "More than 2 children on "
                    print parent
                    print "While adding"
                    print self.it
                    print "\n"
                    return None
            else:
                #This isn't a new node. It is a node being added in
                    
                if(self.value[1] == leaf):
                    #If this child contains the leaf node you are travelling to
                    self.value[1]=[self.it,leaf,None]
                    self.it+=1
                    return self.it - 1
                    #What we have done is create a new tree strucutre. One branch
                    #Is empty and the other branch contains the current leaf we
                    #are heading to. Basically inserts a node while travelling a tree
                elif(self.value[2]==leaf):
                    self.value[2]=[self.it,leaf,None]
                    self.it+=1
                    return self.it - 1
                else:
                    print "Error Occurered, the Lead node passed in wasn't detected"
                    print leaf
                    return None

test = tree()
test.add_node(0)
test.add_node(0)
test.add_node(1)
print test.value