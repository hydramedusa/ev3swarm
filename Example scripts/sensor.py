#!/usr/bash/python

import ev3dev.ev3 as ev3

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
gyro  = ev3.GyroSensor()
color = ev3.ColorSensor()

ultra.mode = ultra.MODE_US_DIST_CM
gyro.mode  = gyro.MODE_GYRO_ANG
color.mode = color.MODE_COL_COLOR


while(True):
    print("Touch Sensor")
    print(touch.value(0))
    print("Light Sensor")
    print(color.value())
    print("Gyro Sensor")
    print(gyro.value(0))
    print("Ultra Sonic Sensor")
    print(ultra.value(0))
    