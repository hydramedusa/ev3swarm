#!/usr/bash/python

#CURRENT ISSUES: Turning left causes splits to be registered

import ev3dev.ev3 as ev3

HIGH_SPD = 75
LOW_REV_SPD = 30
HIGH_REV_SPD = 30

# Double light sensor
#0 No color
#1 Black
#2 Blue
#3 Green
#4 Yellow
#5 Red
#6 White
#7 Brown
WHITE = 6

leftMotor = ev3.LargeMotor('outB'); assert leftMotor.connected
rightMotor = ev3.LargeMotor('outC'); assert rightMotor.connected
leftMotor.duty_cycle_sp = 0
rightMotor.duty_cycle_sp = 0
leftMotor.run_direct()
rightMotor.run_direct()


leftColour = ev3.ColorSensor('in1'); assert leftColour.connected
centreColour = ev3.ColorSensor('in3'); assert centreColour.connected

leftColour.mode = leftColour.MODE_COL_COLOR
centreColour.mode = centreColour.MODE_COL_COLOR

buttons = ev3.Button()

atSplit = False
splitCount = 0

leftMotor.duty_cycle_sp = HIGH_SPD
rightMotor.duty_cycle_sp = HIGH_SPD
while True:
    if buttons.any():
        leftMotor.duty_cycle_sp = 0;
        rightMotor.duty_cycle_sp = 0;
        break

    centreC = centreColour.value()
    leftC = leftColour.value()

    if centreC == WHITE:
        leftMotor.duty_cycle_sp = HIGH_SPD
        rightMotor.duty_cycle_sp = -LOW_REV_SPD

        if leftC == WHITE:
            # If we weren't at a split before, then this is new split
            if not atSplit:
                atSplit = True
                splitCount += 1
                print "Splits=%d" % (splitCount)

            rightMotor.duty_cycle_sp = -HIGH_REV_SPD
        else:
            atSplit = False
    else:
        leftMotor.duty_cycle_sp = -LOW_REV_SPD
        rightMotor.duty_cycle_sp = HIGH_SPD