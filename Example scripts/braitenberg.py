#!/usr/bash/python

import ev3dev.ev3 as ev3

HIGH_SPD = 50
LOW_SPD = 25

# Double light sensor
#0 No color
#1 Black
#2 Blue
#3 Green
#4 Yellow
#5 Red
#6 White
#7 Brown
WHITE = 6

leftMotor = ev3.LargeMotor('outB')
rightMotor = ev3.LargeMotor('outC')
leftMotor.duty_cycle_sp = 0
rightMotor.duty_cycle_sp = 0
leftMotor.run_direct()
rightMotor.run_direct()


leftColour = ev3.ColorSensor('in1')
rightColour = ev3.ColorSensor('in3')

leftColour.mode = leftColour.MODE_COL_COLOR
rightColour.mode = rightColour.MODE_COL_COLOR

buttons = ev3.Button()

atSplit = False
splitCount = 0

leftMotor.duty_cycle_sp = HIGH_SPD
rightMotor.duty_cycle_sp = HIGH_SPD
while True:
    if buttons.any():
        leftMotor.duty_cycle_sp = 0;
        rightMotor.duty_cycle_sp = 0;
        break
    
    # Both whites implies a road split
    if leftColour.value() == WHITE and rightColour.value() == WHITE:
        # If we weren't at a split before, then this is new split
        if not atSplit:
            atSplit = True
            splitCount += 1
            print "Splits=%d" % (splitCount)

        # Bank left at splits
        leftMotor.duty_cycle_sp = -LOW_SPD
        rightMotor.duty_cycle_sp = HIGH_SPD
    else:
        atSplit = False

        # Left see white, turn left
        if leftColour.value() == WHITE:
            leftMotor.duty_cycle_sp = -HIGH_SPD
            rightMotor.duty_cycle_sp = HIGH_SPD
        # Left see white, turn right
        elif rightColour.value() == WHITE:
            leftMotor.duty_cycle_sp = HIGH_SPD
            rightMotor.duty_cycle_sp = -HIGH_SPD
        else:
            leftMotor.duty_cycle_sp = HIGH_SPD
            rightMotor.duty_cycle_sp = HIGH_SPD