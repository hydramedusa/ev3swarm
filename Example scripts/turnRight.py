#!/usr/bash/python

import ev3dev.ev3 as ev3

#Color to turn on
#0 No color
#1 Black
#2 Blue
#3 Green
#4 Yellow
#5 Red
#6 White
#7 Brown
color = 6

leftmotor = ev3.LargeMotor('outB')
rightmotor = ev3.LargeMotor('outC')

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
gyro  = ev3.GyroSensor()
color = ev3.ColorSensor()

ultra.mode = ultra.MODE_US_DIST_CM
gyro.mode  = gyro.MODE_GYRO_ANG
color.mode = color.MODE_COL_COLOR

while(True):
    if( touch.value(0) == 1 ):
        rightmotor.stop()
        leftmotor.stop()
        quit()
    
    if( color.value() != 6):
        rightmotor.stop()
        leftmotor.run_forever(duty_cycle_sp=50)
    else:
        leftmotor.run_forever(duty_cycle_sp=20)
        rightmotor.run_forever(duty_cycle_sp=50)


    