#!/usr/bin/env python

#Need to explore the map and build a tree while we are at it


#!/usr/bash/python

import ev3dev.ev3 as ev3
#Color to turn on

leftmotor = ev3.LargeMotor('outB')
rightmotor = ev3.LargeMotor('outC')

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
gyro  = ev3.GyroSensor()
color = ev3.ColorSensor()

ultra.mode = ultra.MODE_US_DIST_CM
gyro.mode  = gyro.MODE_GYRO_ANG
color.mode = color.MODE_COL_COLOR

state = 1 #0 = left 1 = straight 2 = right

minspeed = 0;
maxspeed = 100;
speed = 20;
ratio = 1;
count = 0;
curr = 0;
prev = 0;
def veerRight():
    rightmotor.run_forever(duty_cycle_sp=50)
    leftmotor.run_forever(duty_cycle_sp=60)
    
def veerLeft():
    rightmotor.run_forever(duty_cycle_sp=60)
    leftmotor.run_forever(duty_cycle_sp=50)
    
def turnRight():
    rightmotor.run_forever(duty_cycle_sp=-30)
    leftmotor.run_forever(duty_cycle_sp=50)
    
def turnLeft():
    rightmotor.run_forever(duty_cycle_sp=50)
    leftmotor.run_forever(duty_cycle_sp=-30)
    
def runMotor(speed, ratio):
    rightmotor.run_forever(duty_cycle_sp=speed)
    leftmotor.run_forever(duty_cycle_sp=speed*ratio)
    

while(True):
    if( touch.value(0) == 1 ):
        rightmotor.stop()
        leftmotor.stop()
        quit()
    curr = color.value()
    if( curr != 6):
        if (prev == curr ):
            count++
            ratio = ratio+0.1
    else:
        if( prev == curr):
            count++
            ration = ration-0.1
    
    if(curr!=prev && count < 3):
        speed = speed+0.1
    else:
        speed = speed-0.1    
    
    prev = curr
    runMotor(speed, ratio)


    