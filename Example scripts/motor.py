#!/usr/bin/python

# import ev3dev.ev3 as ev3
# m.run_timed(time_sp=3000, speed_sp=500)


from ev3dev.auto import *

# Connect two large motors on output ports B and C:
motors = [LargeMotor(address) for address in (OUTPUT_B, OUTPUT_C)]

# Every device in ev3dev has `connected` property. Use it to check that the
# device has actually been connected.
assert all([m.connected for m in motors]), \
    "Two large motors should be connected to ports B and C"

m.run_timed(time_sp=3000, speed_sp = 500)
for m in motors:
    m.run_direct()

for m in motors:
    m.run_timed(duty_cycle_sp=75, time_sp=5)
