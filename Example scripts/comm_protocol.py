#!/usr/bin/env python

#Imports
import ev3dev.ev3 as ev3
import time

#Set up the sensors I will need and their sensors

debug_on = True

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
color = ev3.ColorSensor()

ultra.mode = ultra.MODE_US_DIST_CM
color.mode = color.MODE_COL_COLOR

def backwardforward():
    leftmotor.run_to_rel_pos(100)
    rightmotor.run_to_rel_pos(100)
    set_time = time.time()
    while(set_time+2>time.time()):
        #While loop to let the motor run, not sure if it blocks
    leftmotor.run_to_rel_pos(-100)
    rightmotor.run_to_rel_pos(-100)
    set_time = time.time()
    while(set_time+2>time.time()):
        #While loop to let the motor run, not sure if it blocks
    

def forwardbackward():
    leftmotor.run_to_rel_pos(-100)
    rightmotor.run_to_rel_pos(-100)
    set_time = time.time()
    while(set_time+2>time.time()):
        #While loop to let the motor run, not sure if it blocks
    leftmotor.run_to_rel_pos(100)
    rightmotor.run_to_rel_pos(100)
    set_time = time.time()
    while(set_time+2>time.time()):
        #While loop to let the motor run, not sure if it blocks
    


def debug(message):
    if( debug_on == True):
        print message

def bump_from_behind():
    #Function that will be called if the robot received a bump
    
    debug("Bumped from behind, stopping")
    stop_movement() #Stop all the robots movement
    
    set_time = time.time()
    press = 0
    
    debug("Waiting for conversation confirmation")
    while(set_time+5 > time.time() and press<2):
        #Wait to see if there are 2 presses for confirmation
        
        if( touch.value(0) == 1 and press >= 0 ):
            #If the button is registerd as a press and the press value
            #is greater than 0, we update. We set it negative so that
            #We wont read in a billion values as the value from button
            #Will keep sending one until it is released
            debug("Registered a button press")
            press = -press #Was positive, make it negative
            press -= 1  #subtract 1
            
        if( touch.value(0) == 0 and press < 0):
            debug("Button released, waiting for next press")
            press = -press; #Was negative, set to positive, so register the button was released
    debug("There is a robot that wants to talk, waiting for commencement")
    #We have confirmed the robot wants to talk to us
    #Wait for it to set the bottom to zero and then we can start talking
    
    while(touch.value(0) != 0):
        #While loop to wait for the robot to trigger the sensor again
    debug("Commending message sending")
    #Once the sensor is triggered, let it stay triggered,
    #Move back and forward to deliver the message
    
    #sample message, needs to be broken down #also need to make sure it triggers something
    forwardbackward()
    forwardbackward()
    forwardbackward()
    
    #The receiver must time out

#Dont worry about front case for now
def bump_infront():
    #Bump the robot infront of you, this will be sort of weird
    
    #I assume the robot is in front of it
    #Need to keep it on the track, just

#If we are activtely talking to every ant we run into, we need a different method of moving

#If you detect you are not moving (no ultra, just wheel readings)
#Enter into this

#THIS DOESN"T MAKE SENSE ANYMORE
def bump_front():
    #robots need to approach each other until the ultra sensor isn't getting a reading
    
    front_front_manouver() #Special manouver function for 2 robots
    #that are running into reach other, they wil lreverse at an angle
    #Then attempt to line up their sensors and move together
    
    #I have assumed they are lined up from front_front_manouver
    #Assumption that the sensors are lined up and are less than 100 away
    while(ultra.value(0) < 100 or ultra.value(0) == 2550):
        #2550 is equivilant to 0 or max range.
        #There are small differences in what the robot has to do if it is faicng the root or not
        #Who ever is facing the root, should have more information
        #And hence will attempt to initialize the message
        if( facing == True):
            #Need to check the reading values
            backwardforward()
            backwardforward()
        else:
            set_time = time.time()
            reading = 0
            while(set_time+10>time.time()):
                if( ultra.value(0) == 2550 and reading >= 0):
                    reading = -reading
                    reading-=1
                if( ultra.value(0)<= 100 and reading < 0):
                    reading = -reading
            
    
    #CHannge gyro mode mode from MODE_US_DIST_CM to MODE_US_LISTEN
    
    
#MADE A CHANGE TO THE core.py file changed lego_port to lego-port
#To call a port use ev3.LegoPort("PortName") e.g. ev3.LegoPort("in4")