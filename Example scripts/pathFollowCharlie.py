#!/usr/bin/python

import ev3dev.ev3 as ev3


# This follows the left border of the path
# Assumes colour sensor is on the right side of the EV3
# Follows the midpoint between two colours using an intensity sensor
# Uses a PID controller

def main():

    # Sensor values
    white = 45
    black = 11
    middle = (white-black)/2 + black
    intensityRange = white-black
    turnThreshold = 0.1 # If detected intensity is in the bottom or top percent of the range, we turn
    # Controller values
    kp = 0.5;
    ki = 0.01;
    kd = 0.1;
    prevError = 0
    integral = 0
    # Motor values
    leftMotor = ev3.LargeMotor('outB')
    rightMotor = ev3.LargeMotor('outC')
    startSpeed = 40
    maxSpeed = 100
    leftMotorSpeed = rightMotorSpeed = startSpeed

    # Initialise sensors
    touch = ev3.TouchSensor()
#    ultra = ev3.UltrasonicSensor()
    gyro  = ev3.GyroSensor()
    color = ev3.ColorSensor()

#    ultra.mode = ultra.MODE_US_DIST_CM
    gyro.mode  = gyro.MODE_GYRO_ANG
    color.mode = color.MODE_COL_REFLECT

    # Assuming clockwise positive
    gyroTurnThreshold = 58
    gyroDeadendThreshold = 140
    bufferSize = 150
    bufferSublistSize = 5
    turnIndex = 60
    gyroBuffer = [gyro.value(0)]*bufferSize
    travelForwardCounter = 0

    # 180 degree turn stuff
    testTurn = False

    # Control loop
    # Quit if we tell it to via the touch sensor
    while(touch.value(0) == 0):

        # Update the gyro buffer
        gyroBuffer.pop()
        gyroBuffer.insert(0,gyro.value(0))
        
        if testTurn:
            # Turn unless we detect that we have done a 180
            currGyroBuffer = gyroBuffer[:bufferSublistSize]
            currGyro = mean(currGyroBuffer)
            prevGyroBuffer = gyroBuffer[-bufferSublistSize:]
            prevGyro = mean(prevGyroBuffer)
            if(currGyro-prevGyro >= 120):
                # We did a 180
                print "Finished U-turn!"
                gyroBuffer = [gyro.value(0)]*bufferSize
                travelForwardCounter = 0
                testTurn = False
            else:
                if travelForwardCounter < 20:
                    rightMotorSpeed = startSpeed
                    leftMotorSpeed = startSpeed
                    rightMotor.run_forever(duty_cycle_sp=rightMotorSpeed)
                    leftMotor.run_forever(duty_cycle_sp=leftMotorSpeed)
                    travelForwardCounter = travelForwardCounter + 1
                else:
                    rightMotorSpeed = 0
                    leftMotorSpeed = startSpeed
                    rightMotor.run_forever(duty_cycle_sp=rightMotorSpeed)
                    leftMotor.run_forever(duty_cycle_sp=leftMotorSpeed)

        else:
            # Detect turns and dead ends
            currGyroBuffer = gyroBuffer[:bufferSublistSize]
            currGyro = mean(currGyroBuffer)
            prevGyroTurnBuffer = gyroBuffer[turnIndex-bufferSublistSize:turnIndex]
            prevGyroTurn = mean(prevGyroTurnBuffer)
            prevGyroDeadEndBuffer = gyroBuffer[-bufferSublistSize:]
            prevGyroDeadEnd = mean(prevGyroDeadEndBuffer)
            # Assume the gyro is set to clockwise positive
            if (prevGyroTurn-currGyro >= gyroTurnThreshold):
                # We turned left
                print "Split detected!"
                gyroBuffer = [gyro.value(0)]*bufferSize
                testTurn = True
            elif (currGyro-prevGyroDeadEnd >= gyroDeadendThreshold):
                # We turned 180 to the right
                print "Deadend detected!"
                gyroBuffer = [gyro.value(0)]*bufferSize
            
            # Set new values for this control loop
            intensity = color.value()
            currError = middle - intensity
            integral = integral + currError
            if currError*prevError < 0:
                integral = 0
            derivative = currError - prevError

            # Actual controller
            controlEffort = kp*(currError) + ki*integral + kd*derivative

            prevError = currError
            # Positive error = too much to right (on black)
            # Negative error = too much to left (on white)

            # Turning right
            if (intensity >= white-intensityRange*turnThreshold):
                rightMotorSpeed = 0
                leftMotorSpeed = startSpeed
                # To prevent the turn from messing up our controller, reset values
                prevError = 0
                integral = 0
            # Turning left
            elif (intensity <= black+intensityRange*turnThreshold):
                rightMotorSpeed = startSpeed/2
                leftMotorSpeed = -startSpeed
                # To prevent the turn from messing up our controller, reset values
                prevError = 0
                integral = 0
            else:
                # Cruising
                rightMotorSpeed = clamp(startSpeed+controlEffort,-maxSpeed,maxSpeed)
                leftMotorSpeed = clamp(startSpeed-controlEffort,-maxSpeed,maxSpeed)

            rightMotor.run_forever(duty_cycle_sp=rightMotorSpeed)
            leftMotor.run_forever(duty_cycle_sp=leftMotorSpeed)
            
            #Debug stuff
    #        print "currError: %d, integral: %.2f, derivative: %.2f, leftMotor: %.1f, rightMotor: %.1f \n" % (currError, integral, derivative, leftMotorSpeed, rightMotorSpeed)
    else:
        rightMotor.stop()
        leftMotor.stop()
        quit()

def clamp(n, floor, ceiling):
    return max(floor, min(n, ceiling))

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers),1)

if __name__ == '__main__':
    main()

