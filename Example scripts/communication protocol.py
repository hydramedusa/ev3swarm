#!/usr/bash/python

import ev3dev.ev3 as ev3
import time
from Timer import Timer
#Need to import the timer class I made, not sure how it will work right now

leftmotor = ev3.LargeMotor('outB')
rightmotor = ev3.LargeMotor('outC')

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
gyro  = ev3.GyroSensor()
color = ev3.ColorSensor()

ultra.mode = ultra.MODE_US_DIST_CM
gyro.mode  = gyro.MODE_GYRO_ANG
color.mode = color.MODE_COL_COLOR

#Physical Assumptions of the robot
#The Touch Sensor is placed in the rear of the robot. It will allow robots behind it to communicate with it
#The Ultra sonic sensor is placed in the front and is at the same level as the touch sensor
#Turning 180's will be perfect and everything will line up

#Logic Behind it

#Robot collids with back of another robot
#Ultra sensor detects an object infront of it
#Bump the robot infront triggering the bump sensor (move back to disable to bump)
#Turn around 180
#Bump the front robot again different amount of times to tell if you are ready to talk
#Bump once and the front robot will attempt to provide food instructions, otherwise it will leave, once it leaves the robot will turn around
    #If it doesn't leave, need to communicate the message to it

#Robots collide head on
#Ultra sensor detects object infront
#Stop moving
#Robot that is closer to the root, will turn around presenting its touch sensor
#Once turned around, it will move backwards and forwards until the bump is triggered
#Robot returning triggers bump
#Use same protocol as above to communicate
#Once its done, navigate around backwards robot
#Backwards robot turn around and continue on, or go look for more food

#This will mimic the robot that is going to be bumped from behind
wait_for_confirmation = 0;
ready_to_talk = 0
message_bumps = 0
while (True):
    if( touch.value(0) == 1):
        #Halt Movement, Someone wants to speak to you
        robot.stop_movement()
        wait_for_confirmation = 1;
    
    while(wait_for_confirmation == 1):
        #Waiting for the other robot to turn around and tell us its ready to talk
        if( touch.value(0) == 1):
            ready_to_talk+=1
        
        if(ready_to_talk == 5):
            wait_for_confirmation = 0
            break; #Exit the loop
        #There also needs to be a timer so the robot wont wait forever
        
    while( ready_to_talk > 1):
        #Wait for a set amount of bumps
        #This is timed as well
        if( touch.value(0) == 1):
            message_bumps += 1
        
        #Needs to timeout
    
    if( message_bumps == 1 ):
        #Move into the robot until your bump sensor triggers, and then move backwards and forwards to send a message
    elif(message_bumps == 2):
        #Do something else
    else:
        

#The robot that detects another robot infront of it and attempts to bump it
while (True):
    if( ultra.value(0) == 100 ): #Assuming 100 is something is in range
        leftmotor.stop()
        rightmotor.stop() #First stop movement
    
    while(waiting_confirmation):
        if( if parent is closer to root than my current target, wait for other robot to send signal):
            #This will need to check the root levels of the parent/child targerts.
            #Could also check if the place you are leaving from is higher orlower than where you are going
            
            #Called == Facing
            
            if(ultra.value(0) == certain range around 100):
                #Once you are waiting, the other robot will try and send you a message. Basically by ramming you in the face
                #Once thats been done a set number of times, it should be fine
                #All these message detections need to have a limited amount of time between them, otherwise it can trigger
                #ALot of times due to it being in the same position for a set amount of time
                message_confirmation = 1
        else:
            #bump the front robot a bit by ramming it a couple times
    
    while(message_confirmation):
        #The closer to robot will now turn around and present its butt, turn 180 degrees
        
        #The other robot, will determine the message to send in order of importance
        #   #Food source + Location, place to explore, or let it time out and keep going back. If you let it time out the robot should not that
        
    while(message_sent):
        #THe further away robot, will navigate around the closer robot and head back to root
        #The closer robot will wait a set time or until its sensors get triggered before proceeding based on the message
    


#Need some way to read the sensor without triggering the response multiple times
#I can't just read the value from the sensor straigh away. Some times I need to read it multiple times and ensure that there was a change so a time difference doesn't help either
#I need to be able to read the signal, see that it has changed, and then read it and check if its crossed the value again