#!/usr/bin/python
from ev3dev.auto import OUTPUT_B, Motor
from ev3dev.auto import INPUT_1, Sensor

import ev3dev.ev3 as ev3
import time

ts = ev3.TouchSensor()
m=Motor(OUTPUT_B)
n=Sensor(INPUT_1)
while True:
    if n.value():
        m.run_forever(duty_cycle_sp=100)
        time.sleep(1)
        m.stop()
