#!/usr/bin/python

import ev3dev.ev3 as ev3

#Color to turn on
#0 No color
#1 Black
#2 Blue
#3 Green
#4 Yellow
#5 Red
#6 White
#7 Brown

# This follows the left border of the path
# Assumes colour sensor is on the right side of the EV3
# Keep count of how long we have seen a colour
# If it exceeds a threshold, we have seen it for too long and we need to turn
# Residue lets us remember how hard we turned and to adjust for it when we finally find the path again

def main():

    colorToFollow = 6
    leftMotor = ev3.LargeMotor('outB')
    rightMotor = ev3.LargeMotor('outC')
    startSpeed = 40
    maxSpeed = 100
    leftMotorSpeed = rightMotorSpeed = startSpeed

    touch = ev3.TouchSensor()
    ultra = ev3.UltrasonicSensor()
    gyro  = ev3.GyroSensor()
    color = ev3.ColorSensor()

    ultra.mode = ultra.MODE_US_DIST_CM
    gyro.mode  = gyro.MODE_GYRO_ANG
    color.mode = color.MODE_COL_COLOR

    count = 0
    turnThreshold = 12
    splitThreshold = 50
    # Assuming clockwise positive
    gyroThreshold = 60
    rampUp = 1.02
    residue = 0
    prevOnPath = (color.value() == colorToFollow)
    bufferSize = 150
    bufferSublistSize = 5
    gyroBuffer = [gyro.value(0)]*bufferSize

    # Quit if we tell it to via the touch sensor
    while(touch.value(0) == 0):
        #Debug stuff
#        print "Count: %d, residue: %.2f, leftMotor: %.1f, rightMotor: %.1f \n" % (count, residue, leftMotorSpeed, rightMotorSpeed)

        # Update the gyro buffer
        gyroBuffer.pop()
        gyroBuffer.insert(0,gyro.value(0))

        #Check if we have changed states of being on the path or not
        currentOnPath = (color.value() == colorToFollow)
        if (prevOnPath != currentOnPath):
            # We switched
            prevOnPath = currentOnPath
            residue = float(clamp(count,0,startSpeed))/2
            count = 0
            leftMotorSpeed = rightMotorSpeed = startSpeed

        count+=1

        # Determine if we should be turning or cruising
        if count > turnThreshold:
            if count == turnThreshold+1:
                # first time above threshold
                leftMotorSpeed = startSpeed

            # Split detection
            currGyroBuffer = gyroBuffer[:bufferSubListSize]
            currGyro = mean(currGyroBuffer)
            prevGyroBuffer = gyroBuffer[-bufferSubListSize:]
            prevGyro = mean(prevGyroBuffer)
            if (count == splitThreshold):
                # Either a split or a dead end
                if (currentOnPath and prevGyro-currGyro >= gyroThreshold):
                    print "Split found!"
                elif (~currentOnPath and currGyro-prevGyro <= -gyroThreshold):
                    print "Dead end found!"

            # If we are currently looking at the path
            if(currentOnPath):
                leftMotorSpeed = -1*clamp(abs(leftMotorSpeed*rampUp),-maxSpeed,maxSpeed)

            # If we are currently looking off the path
            else:
                leftMotorSpeed = clamp(leftMotorSpeed*rampUp,-maxSpeed,maxSpeed)
            
            rightMotorSpeed = 0
            rightMotor.run_forever(duty_cycle_sp=rightMotorSpeed)
            leftMotor.run_forever(duty_cycle_sp=leftMotorSpeed)


        else:
            # If we are currently looking at the path
            if(currentOnPath):
                rightMotorSpeed +=(1+float(residue))
                leftMotorSpeed -=(1+float(residue))

            # If we are currently looking off the path
            else:
                rightMotorSpeed -=(1+float(residue))
                leftMotorSpeed +=(1+float(residue))

            rightMotor.run_forever(duty_cycle_sp=clamp(rightMotorSpeed,-maxSpeed,maxSpeed))
            leftMotor.run_forever(duty_cycle_sp=clamp(leftMotorSpeed,-maxSpeed,maxSpeed))
            residue = float(residue)/2
    else:
        rightMotor.stop()
        leftMotor.stop()
        quit()

def clamp(n, floor, ceiling):
    return max(floor, min(n, ceiling))

def mean(numbers):
    return float(sum(numbers)) / max(len(numbers),1)

if __name__ == '__main__':
    main()

