#!/usr/bin/env python

import time
import ev3dev.ev3 as ev3

class touchListener(object):
    
    interval = 0.3
    timer = 0
    
    probing = 0
    talking = 0
    communicate = 0
    message = []
    
    debug = 0
    def __init__(self,ant):
        #self.ant  ant
        self.ant.touchMotor = ev3.Motor('outA')
        
    def go_to_zero(self):
        self.ant.touchMotor.run_to_abs_pos(position_sp = 90)
    
    def go_to_one(self):
        self.ant.touchMotor.run_to_abs_pos(position_sp = -90)
    
    def go_to_default(self):
        self.ant.touchMotor.run_to_abs_pos(position_sp = -30)
        
    def prob_look(self):
        if( self.ant.ultra.value(0) < 300 and self.talking == 0 and self.communicate == 0):
            self.any.touchMotor.stop_command = 'hold'
            self.probing = 1
            if( abs(abs(self.any.touchMotor.position) - 30) > 10):
                self.go_to_default()
            self.timer = time.time() + 5
            
            if( self.debug == 1):
                print "I am Detecting something"
    
    def prob_check(self):
        if( self.probing == 1 and self.timer < time.time()):
            self.probing = 0
            self.go_to_zero()
            
            if( self.debug == 1):
                print "Nothing was found in the time frame, resetting"
    
    def touching_tips(self):
        if( self.probing == 1 and self.ant.touch.value(0) == 1):
            self.probing = 0
            self.talking = 1
            self.timer = time.time()+0.5
            
            if( self.debug == 1):
                print "Touched tips with another robot"
    
    def confirm_tips(self):
        if( self.talking == 1):
            if( self.ant.touch.value(0) == 1 and self.timer > time.time()):
                self.communicate = 1
                self.talking = 0
                self.timer = time.time()+interval
                
                if( self.debug == 1):
                    print "Confirmed I am touching tips"
        else:
            self.talking = 0
            print "I guess I wasn't touching tips"
    
    def communicate(self):
        if( self.communicate == 1 ):
            self.go_to_default()
            self.timer = time.time() + self.interval*2
        while( cself.ommunicate == 1 ):
            if( self.timer < time.time()):
                self.message.append(0)
                self.timer = time.time() + self.interval*2
                
                if( self.debug == 1):
                    print "Received a 0"
            if( self.ant.touch.value(0) == 1):
                self.message.append(1)
                while(self.ant.touch.value(0) == 1):
                    self.time.sleep(0.001)
                self.timer = time.time() + self.interval*2
                
                if( self.debug == 1):
                    print "Received a 1"
            
            if( len(self.message) > 10):
                if( self.message[-5:] == [0, 0, 0, 0, 0,]):
                    self.communicate = 0
                    print self.message
                    self.message = []
                    self.ant.touchMotor.stop_command = 'brake'
                    self.go_to_zero()
    
    def run_mode(self):
        while(True):
            self.prob_look()
            self.prob_check()
            self.touching_tips()
            self.confirm_tips()
            self.communicate()