#!/usr/bin/env python

import ev3dev.ev3 as ev3
import time

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
touchMotor = ev3.Motor('outA')
leftMotor = ev3.Motor('outB')
rightMotor = ev3.Motor('outC')

ultra.mode = ultra.MODE_US_DIST_CM

touchMotor.position = 0
touchMotor.duty_cycle_sp = 50 #Setting duty cycle so it can run
touchMotor.run_to_abs_pos(position_sp = 0) #Ready to talk
touchMotor.run_to_abs_pos(position_sp = -90) #Not talking

leftMotor.duty_cycle_sp = 50
rightMotor.duty_cycle_sp = 50


def activate_touch():
    touchMotor.run_to_abs_pos(position_sp = -30)

def deactivate_touch():
    touchMotor.run_to_abs_pos(position_sp=-90)

def stop():
    leftMotor.stop()
    rightMotor.stop()
    
def gearD():
    leftMotor.duty_cycle_sp = 50
    rightMotor.duty_cycle_sp = 50

def gearR():
    leftMotor.duty_cycle_sp = -50
    rightMotor.duty_cycle_sp = -50

def go():
    leftMotor.run_forever()
    rightMotor.run_forever()

def tap_once():
    touchMotor.run_to_abs_pos(position_sp = 0)
    time.sleep(0.1)
    touchMotor.run_to_abs_pos(position_sp = -30)
    time.sleep(0.1)

def drive_tap_once():
    while(touch.value(0) == 1):
        gearR()
        go()
    stop()
    while(touch.value(0) == 0):
        gearD()
        go()
    stop()
    
touchS = 0
delay = 0
communicating = 0
message = [-1, -1, -1, -1, -1, -1, -1, -1]

while(True):
    if( ultra.value(0) <= 300 and touchS == 0 and delay <= time.time()):
        activate_touch()
        touchS = 1
        delay = time.time()+5
    elif(ultra.value(0)>300 and touchS == 1 and delay <= time.time()):
        deactivate_touch()
        touchS = 0
        delay = time.time()+0.5
        
    if( touch.value(0) == 1 and touchS == 1 and delay > time.time()):
        print "Something tapped me, initiating communication protocol"
        #delay = time.time()+5
        
        touchMotor.run_to_abs_pos(position_sp = -30) #put the arm in listening position
        delay = time.time()+0.3
        index = 0
        while(True):
            if( delay > time.time() and touch.value(0) == 1):
                #Button has been pressed in the right time, register as a message
                message[index] = 1
                index += 1
                while(touch.value(0) == 1):
                    #do nothing, waiting for trigger to end
                delay = time.time()+0.3
            elif(delay < time.time() and touch.value(0) == 0):
                message[index] = 0
                index += 1
                delay = time.time()+0.3
            
            if(index >= 8):
                print message
                break
        
        gearR()
        go()
        time.sleep(0.3)
        stop()
        deactivate_touch()
        touchS = 0
    
