#!/usr/bin/env python

import time
import ev3dev.ev3 as ev3

communication_delay = 1 #Delay when I wont receive messages after reading a value (should really confirm multiple sends)
communication_limit = 10 #If you dont recieve communications in 10 seconds, stop

#class Ultra_Talker:
to_listen = 0
ultra = None
count_down = 0
message = 0
init_message = 0
#    def __init__(self):
to_listen = 0
ultra = ev3.UltrasonicSensor()
ultra.mode = ultra.MODE_US_LISTEN #Set to listen so it wont send a contant stream of messages
count_down = 0
message = 5
init_message = 0

#    def wait(self, wait_period = 0):
#        timer = time.time()+wait_period
#        while(time > time.time()):
#            sleep(0.01)
    
#    def send_message(self, message = 1)
#while( message > 0):
#    print message
#    ultra.mode = ultra.MODE_US_DIST_CM
#    message -= 1
#    time.sleep(2.5)
#    ultra.mode = ultra.MODE_US_LISTEN
#    time.sleep(0.1)
    
#Talk with 0 1 messages

def send_one():
    print "Sending One"
    ultra.mode = ultra.MODE_US_DIST_CM
    time.sleep(2.5)
    ultra.mode = ultra.MODE_US_LISTEN
    time.sleep(0.1)

def send_zero():
    print "Sending Zero"
    ultra.mode = ultra.MODE_US_LISTEN
    time.sleep(2.6)

#Initial one to kick start the message
message = [1, 1, 1, 1, 0, 1, 1, 1, 0]

for index in range(len(message)):
    if( message[index] == 1):
        send_one()
    else:
        send_zero()
    
    