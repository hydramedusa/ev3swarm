#!/usr/bin/env python

import ev3dev.ev3 as ev3
import time

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
touchMotor = ev3.Motor('outA')
leftMotor = ev3.Motor('outB')
rightMotor = ev3.Motor('outC')

touchMotor.stop_command = "brake"

interval = 0.3 #This is T
timer = 0
#ultra.mode = ultra.MODE_US_DIST_CM

#touchMotor.position = 0
#touchMotor.duty_cycle_sp = 50 #Setting duty cycle so it can run
#touchMotor.run_to_abs_pos(position_sp = 0) #Ready to talk
#touchMotor.run_to_abs_pos(position_sp = -90) #Not talking

#leftMotor.duty_cycle_sp = 50
#rightMotor.duty_cycle_sp = 50


def init():
    touch = ev3.TouchSensor()
    ultra = ev3.UltrasonicSensor()
    
    leftMotor = ev3.Motor('outB')
    rightMotor = ev3.Motor('outC')
    
    touchMotor = ev3.Motor('outA')
    
    ultra.mode = ultra.MODE_US_DIST_CM
    
    touchMotor.position = 0
    touchMotor.duty_cycle_sp = 50;

def go_to_zero():
    touchMotor.run_to_abs_pos(position_sp = 90)

def go_to_one():
    touchMotor.run_to_abs_pos(position_sp = -90)

def go_to_default():
    touchMotor.run_to_abs_pos(position_sp = -30)


    #send one and zero need to be adjusted to be better
def send_one():
    #Attempt to go to its abs one position, until it no longer triggers the touch sensor
    #Then return to zero position
    timer = time.time() + interval
    #Now we run it until it has triggered and then untriggered
    while(touch.value(0) == 0):
        if( touchMotor.position > -90):
            touchMotor.run_forever(duty_cycle_sp = -50)
        else:
            touchMotor.stop()
#    if(touch.value(0) == 1):
    while(touch.value(0) == 1 and timer > time.time()):
        if( touchMotor.position > -90):
            touchMotor.run_forever(duty_cycle_sp = -20)
        else:
            touchMotor.stop()
                 
    touchMotor.stop()   
    #if( touch.value(0) == 0):
    while(timer > time.time()):
        time.sleep(0.01) #ensure that it has taken atleast 1T to move forward
                        
    touchMotor.duty_cycle_sp = 60
    go_to_zero()
    #time.sleep(interval)
    while(timer + interval >time.time()):
        time.sleep(0.01) #ensure that when it reaches zero, anothe 1T has passed

    
def send_zero():
    go_to_zero()
    time.sleep(interval*2) #now sure about the timeing for now


    #Assume the left and right motor already have assigned duty cycles
probing = 0 #Waiting for touch
talking = 0 #Touched and waiting set time to make sure
communicate = 0
init() #initialise some stuff?

message = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
message = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1]
while(True):
    if( ultra.value(0) < 300 and talking == 0 and talking == 0 and communicate == 0):
        probing = 1
        go_to_default()
        timer = time.time()+5
        print "I am detecting something"
    
    if( probing == 1 and timer < time.time()):
        probing = 0
        go_to_zero()
        print "Nothing found in the time frame, resetting"
    
    if( probing == 1 and touch.value(0) == 1):
        probing = 0
        talking = 1 #robots have touched, wait a bit
        time.sleep(0.5)
        print "Initial touch confirmed, waiting"

    if( talking == 1):
        if( touch.value(0) == 1):
            communicate = 1
            talking = 0
            go_to_zero()
            print "Touch confirmed, proceeding with coms"
        else:
            talking = 0
            print "Touch failed, resetting"
            
    
    if( communicate == 1):
        for index in range(len(message)):
            if( message[index] == 1):
                send_one()
                print "sent one"
            else:
                send_zero()
                print "sent zero"
        
        communicate = 0
        go_to_zero()
        time.sleep(2)