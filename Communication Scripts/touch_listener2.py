#!/usr/bin/env python

import ev3dev.ev3 as ev3
import time

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
touchMotor = ev3.Motor('outA')
leftMotor = ev3.Motor('outB')
rightMotor = ev3.Motor('outC')

touchMotor.stop_command = "hold"


interval = 0.3 #This is T
timer = 0
#ultra.mode = ultra.MODE_US_DIST_CM

#touchMotor.position = 0
#touchMotor.duty_cycle_sp = 50 #Setting duty cycle so it can run
#touchMotor.run_to_abs_pos(position_sp = 0) #Ready to talk
#touchMotor.run_to_abs_pos(position_sp = -90) #Not talking

#leftMotor.duty_cycle_sp = 50
#rightMotor.duty_cycle_sp = 50


def init():
    touch = ev3.TouchSensor()
    ultra = ev3.UltrasonicSensor()
    
    leftMotor = ev3.Motor('outB')
    rightMotor = ev3.Motor('outC')
    
    touchMotor = ev3.Motor('outA')
    
    ultra.mode = ultra.MODE_US_DIST_CM
    
    touchMotor.position = 0
    touchMotor.duty_cycle_sp = 50;

def go_to_zero():
    touchMotor.run_to_abs_pos(position_sp = 90)

def go_to_one():
    touchMotor.run_to_abs_pos(position_sp = -90)

def go_to_default():
    touchMotor.run_to_abs_pos(position_sp = -30)

    #Assume the left and right motor already have assigned duty cycles
probing = 0 #Waiting for touch
talking = 0 #Touched and waiting set time to make sure
communicate = 0
init() #initialise some stuff?

message = []

while(True):
    #Small bit of bouncing when it hits the position or near the position
    #Need to add a timeout to reset it to the default position
    if( ultra.value(0) < 300 and talking == 0 and communicate == 0):
        touchMotor.stop_command = 'hold'
        probing = 1
        if( abs(abs(touchMotor.position) - 30) > 10 ):
            go_to_default()
        timer = time.time() + 5
        print "I am detecting something"
    
    if( probing == 1 and timer < time.time()):
        probing = 0
        go_to_zero()
        print "Nothing found in the time frame, resetting"
    
    if( probing == 1 and touch.value(0) == 1 ):
        probing = 0
        talking = 1 #robots have touched, wait a bit
        timer = time.time()+0.5
        print "Initial Touch confirmed, waiting"

    if( talking == 1):
        if( touch.value(0) == 1 and timer>time.time()):
            communicate = 1
            talking = 0
            timer = time.time()+interval #starting the communication now
            print "Touch confirmed, proceeding with coms"
        else:
            talking = 0
            print "Touch failed, resetting"
            
    
    #this needs to terminate somehow, looping forever right now
    value = 0
    if( communicate == 1):
        go_to_default()
        timer = time.time()+interval*2
    while(communicate == 1):
        if( timer < time.time()):
            message.append(0)
            timer = time.time()+interval*2
            print "0"
        if( touch.value(0) == 1):
            message.append(1)
            print "1"
            while(touch.value(0) == 1):
                time.sleep(0.001)
            timer = time.time()+interval*2
        
        if( len(message) > 10):
            if( message[-5:] == [0, 0, 0, 0, 0,]):
                communicate = 0
                print message
                message = []
                touchMotor.stop_command = 'brake'
                go_to_zero()
        #if( touch.value(0) == 0 ):
        #    if( touchMotor.position != -30): #default pos
        #        go_to_default()
        #    else:
        #        if( timer < time.time()):
        #            print message
        #            message.append(value)
        #            value = 0
        #            timer = time.time()+interval
        #else:
        #    touchMotor.run_forever(dutycycle_sp = 20)
        #    value = 1