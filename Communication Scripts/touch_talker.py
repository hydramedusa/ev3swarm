#!/usr/bin/env python

import ev3dev.ev3 as ev3
import time

touch = ev3.TouchSensor()
ultra = ev3.UltrasonicSensor()
touchMotor = ev3.Motor('outA')
leftMotor = ev3.Motor('outB')
rightMotor = ev3.Motor('outC')

ultra.mode = ultra.MODE_US_DIST_CM

touchMotor.position = 0
touchMotor.duty_cycle_sp = 50 #Setting duty cycle so it can run
touchMotor.run_to_abs_pos(position_sp = 0) #Ready to talk
touchMotor.run_to_abs_pos(position_sp = -90) #Not talking

leftMotor.duty_cycle_sp = 50
rightMotor.duty_cycle_sp = 50


def activate_touch():
    touchMotor.run_to_abs_pos(position_sp = -30)

def deactivate_touch():
    touchMotor.run_to_abs_pos(position_sp=-90)

def stop():
    leftMotor.stop()
    rightMotor.stop()
    
def gearD():
    leftMotor.duty_cycle_sp = 50
    rightMotor.duty_cycle_sp = 50

def gearR():
    leftMotor.duty_cycle_sp = -50
    rightMotor.duty_cycle_sp = -50

def go():
    leftMotor.run_forever()
    rightMotor.run_forever()

def tap_once():
    touchMotor.run_to_abs_pos(position_sp = 0)
    time.sleep(0.1)
    touchMotor.run_to_abs_pos(position_sp = -30)
    time.sleep(0.1)

def tap_zero():
    touchMotor.run_to_abs_pos(position_sp=0)
    time.sleep(0.2)

def drive_tap_once():
    while(touch.value(0) == 1):
        gearR()
        go()
    stop()
    while(touch.value(0) == 0):
        gearD()
        go()
    stop()
    
touchS = 0
delay = 0
communicating = 0   

message = [0, 1, 1, 1, 1, 0, 1, 1]

while(True):
    if( ultra.value(0) <= 300 and touchS == 0 and delay <= time.time()):
        activate_touch()
        touchS = 1
        delay = time.time()+5
    elif(ultra.value(0)>300 and touchS == 1 and delay <= time.time()):
        deactivate_touch()
        touchS = 0
        delay = time.time()+0.5
        
    if( touch.value(0) == 1 and touchS == 1 and delay > time.time()):
        print "Something tapped me, initiating communication protocol"
        delay = time.time()+5
        while( index < 8):
            for index in range(len(message)):
                if( message[index] == 1 ):
                    tap_once
                else:
                    tap_zero
        
        
        
        gearR()
        go()
        time.sleep(0.3)
        stop()
        deactivate_touch()
        touchS = 0
    
#How will this work
#2 Key areas, the Handshake, and the message. Since this is the talker, the handshake will be focused here.


#For the message to be communicated properly, need to establish when the other robot has received a message
#Solution. Go to the initial set position which should be roughly angle 30
#Keep moving until I am triggered
#Save this trigger position
#Assume at this point, both robots have triggered and there should be some tension
#Have both robots wave until they are triggering along a consistent time? (This will have an adjustment period)
#Once 3 messages have been triggered, send a one, zero one one then start?

target_message_delay = 0.2
message_delay_error = 0.5 #Keep adjusting until the messages are in the error region
arm_delay = 0.2
arm_delay_error = 0.5

initial_touch_wait = 5
time_tracker = 0

triggered = 0
triggered_wait_period = 0.5 #A wait perdio for the initial trigger to make sure both robots have triggered
handshake = 0
handshake_safe = 3 #The number of handshakes in the safe zone for it to count
handshake_safe_value = 0 #The tracker of the current number
communicating = 0

forward_duty = 50
backward_duty = 50

def go_to_zero_pos():
    leftmotor.run_to_abs_pos(position_sp = 0)
    rightmotor.run_to_abs_pos(position_sp = 0)
    
def go_to_one_pos():
    leftmotor.run_to_abs_pos(position_sp = 30)
    rightmotor.run_to_abs_pos(position_sp = 30)
    

initial = 0
while(True):
    if( (ultra.value(0) < 300 or initial == 1) and triggered == 0 and handshake == 0 and communicating == 0):
        if( initial == 0):
            initial == 1
            time_tracker = time.time()+initial_touch_wait #Only set the time on the initial one
        
        if( touch.value(0) == 1):
            #Touch was triggered on this. #stop the motors
            stop() #assume this stops the motors
            triggered = 1
            initial == 0
            time_tracker = 0 #reset this to be used later on
            
    if( triggered == 1 ):
        time.sleep(triggered_wait_period) #let it wait for a bit incase the other robot hasn't triggered yet
        if( touch.value(0) == 0 ):
            #Wasn't triggered properly
            #Might want to do something here
            triggered = 0
        else:
            #It was triggered properly
            triggered == 0
            handshake == 1
    
    if( handshake == 1 ):
        #This should start in the zero position
        
        #This part is more like the sending code, There is a forward and backward position that this will maintain until the message is finally sent
        
        #want to keep adjusting the forward and back positions until we get a trigger and adjust the duty cycle to ensure it happens each time
        #will attempt to not do a while loops on this so that the T-R program can be maintained
        
        trigger_value = 0 #0 for off
        time_gap = 0
        if( trigger_value == 1 and touch.value(0) == 1):
            go_to_zero_pos()
            time_gap = saved_time - time.time()
            
            if( time_gap < arm_delay-arm_delay_error):
                if(forward_duty <= 100 and forward_duty >= 0):
                    forward_duty -= 5 #gap too small decrease
                    safe_zone = 0
            elif( time_gap > arm_delay+arm_delay_error):
                if(forward_duty <= 100 and forward_duty >= 0):
                    forward_duty += 5 #gap too large increase
                    save_zone = 0
            else:
                save_zone += 1
        elif(trigger_value == 0 and touch.value(0) == 0):
            trigger_value = 1
            go_to_one_pos()
            time_gap = saved_time - time.time()
            if( time_gap < arm_delay-arm_delay_error):
                if(backward_duty <= 100 and backward_duty >= 0):
                    backward_duty -= 5 #gap too small decrease
                    safe_zone = 0
            elif( time_gap > arm_delay+arm_delay_error):
                if(backward_duty <= 100 and backward_duty >= 0):
                    backward_duty += 5 #gap too large increase
                    safe_zone = 0
            else:
                safe_zone += 1
    
        
        
        
        
        #start the grace detection period
        if( safe_zone == handshake_safe):
            handshake = 0
            communicating = 1
            #need to set both to send zero position nad instantly start sending message
            
    if( communicating == 1):
        #Now this part I send the message, this part should be pretty simple
    