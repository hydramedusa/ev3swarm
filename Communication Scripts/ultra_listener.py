#!/usr/bin/env python

import time
import ev3dev.ev3 as ev3

communication_delay = 1 #Delay when I wont receive messages after reading a value (should really confirm multiple sends)
communication_limit = 10 #If you dont recieve communications in 10 seconds, stop

#class Ultra_Listener:
to_listen = 0
ultra = None
count_down = 0
message = 0
init_message = 0
 #   def __init__(self):
to_listen = 0
ultra = ev3.UltrasonicSensor()
ultra.mode = ultra.MODE_US_DIST_CM
count_down = time.time()
message = 0
init_message = time.time()

#Start simple
to_listen = 1
ultra.mode = ultra.MODE_US_LISTEN

#Listen to 0 and 1 messages with overall timer

while( ultra.value(0) != 0):
    time.sleep(0.01) #Waiting for start
print "Preparing to talk, send no signal to signal start"
while(ultra.value(0) != 1):
    time.sleep(0.01)
time.sleep(1)
count_down = time.time()+3 #3 seconds to terminate a message
overall_timer = time.time()+21 #should only need roughly 8*3 = 24 seconds
message = [-1,-1,-1,-1,-1,-1,-1,-1]
index = 0
print "Starting to Listen"
while(to_listen == 1):
    if( overall_timer < time.time()):
        print "Waited too long, terminating"
        to_listen = 0
        break;
    
    if (count_down < time.time()):
        print "Recieved a zero"
        count_down = time.time()+3
        message[index] = 0
        index+=1
        
    if( ultra.value(0) == 1):
        print "received a one"
        message[index] = 1
        index+=1
        count_down = time.time()+3
        while(ultra.value(0) == 1):
            if( count_down < time.time()):
                print "Message Terminated, couldn't detect change in signal"
                to_listen = 0
                break
            
            time.sleep(0.01)
print "What I've received so far"
print message

#while(to_listen == 1):
#    if( ultra.value(0) == 1):
#        print "Message Recieved"
#    else:
#        print "No Message ========="

#Listening for a trigger
#while(to_listen == 1):
#    if( ultra.value(0) == 1):
#        print "Pinged, waiting for change to no message"
#        while(ultra.value(0)==1):
#            #This just waits
#            sleep(0.01)
#        
#        print "Changed, restart"

#Listen with a timer that will terminate a message if nothing recieved for a set time (3 seconds)

#while( ultra.value(0) != 0):
#    time.sleep(0.01) #Waiting for start
#print "Preparing to talk, send no signal to signal start"
#while(ultra.value(0) != 1):
#    time.sleep(0.01)
#time.sleep(1)
#count_down = time.time()+3 #3 seconds to terminate a message
#print "Starting to Listen"
#while(to_listen == 1):
#    if (count_down < time.time()):
#        print "Message Terminated, exceeded 3 seconds of time"
#        to_listen = 0
#        break

#    if( ultra.value(0) == 1):
#        print "Message, received, updating timer"
#        count_down = time.time()+3
#        while(ultra.value(0) == 1):
#            if( count_down < time.time()):
#                print "Message Terminated, couldn't detect change in signal"
#               to_listen = 0
#                break
#            
#            time.sleep(0.01)
#        print "changed detected"
#        
    

#Listen with 1,0 messages and overall timer
    
    