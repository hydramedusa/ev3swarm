#!/usr/bin/env python
import time
import ev3dev.ev3 as ev3

class touchTalker(object):
    interval = 0.3
    timer = 0
    
    probing = 0
    talking = 0
    communicate = 0
    message = []
    
    debug = 0
    
    def __init__(self,ant):
        self.ant.touchMotor = ev3.Motor('outA')
    
    def go_to_zero(self):
        self.ant.touchMotor.run_to_abs_pos(position_sp = 90)
    
    def go_to_one(self):
        self.ant.touchMotor.run_to_abs_pos(position_sp = -90)
    
    def go_to_default(self):
        self.ant.touchMotor.run_to_abs_pos(position_sp = -30)
    
    def send_one(self):
        self.timer = time.time() + interval
        
        while(self.ant.touch.value(0) == 0 ):
            if( self.ant.touchMotor.position > -90 ):
                self.ant.touchMotor.run_forever(duty_cycle_sp = -50)
            else:
                self.ant.touchMotor.stop()
        
        while( self.ant.touch.value(0) == 1 and timer > time.time()):
            if( self.ant.touchMotor.position > -90 ):
                self.ant.touchMotor.run_forever(duty_cycle_sp = -20)
            else:
                self.ant.touchMotor.stop()
        self.ant.touchMotor.stop()
        while(self.timer > time.time()):
            time.sleep(0.01)
        
        self.go_to_zero()
        while(self.timer + self.interval > time.time()):
            time.sleep(0.01)
    
    def send_zero()
        self.go_to_zero()
        time.sleep(interval*2)
    
    def prob_look(self):
        if( self.ant.ultra.value(0) < 300 and self.talking == 0 and self.communicate == 0):
            self.any.touchMotor.stop_command = 'hold'
            self.probing = 1
            if( abs(abs(self.any.touchMotor.position) - 30) > 10):
                self.go_to_default()
            self.timer = time.time() + 5
            
            if( self.debug == 1):
                print "I am Detecting something"
    
    def prob_check(self):
        if( self.probing == 1 and self.timer < time.time()):
            self.probing = 0
            self.go_to_zero()
            
            if( self.debug == 1):
                print "Nothing was found in the time frame, resetting"
    
    def touching_tips(self):
        if( self.probing == 1 and self.ant.touch.value(0) == 1):
            self.probing = 0
            self.talking = 1
            self.timer = time.time()+0.5
            
            if( self.debug == 1):
                print "Touched tips with another robot"
    
    def confirm_tips(self):
        if( self.talking == 1):
            if( self.ant.touch.value(0) == 1 and self.timer > time.time()):
                self.communicate = 1
                self.talking = 0
                self.timer = time.time()+self.interval
                
                if( self.debug == 1):
                    print "Confirmed I am touching tips"
        else:
            self.talking = 0
            print "I guess I wasn't touching tips"
    
    def communicate(self):
        if ( self.communicate == 1):
            for index in range(len(message)):
                if( self.message[index] == 1):
                    self.send_one()
                    if( self.debug == 1):
                        print "Sent 1"
                else:
                    self.send_zero()
                    if( self.debug == 1):
                        print "Sent 0"
            
            self.message = [] #this will delete the message so it wont send again
            self.communicate = 0
            self.go_to_zero()
            time.sleep(2)
    
    def set_message(self, message_):
        self.message = message_
    
    def run_mode(self):
        while(True):
            self.set_message([1,0,1,1,0,0,1,1,0,0,1])
            self.prob_look()
            self.prob_check()
            self.touching_tips()
            self.confirm_tips()
            self.communicate()